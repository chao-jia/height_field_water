#ifndef GLHELPER_H
#define GLHELPER_H

#include <fstream>
#include <map>

#include <QOpenGLFunctions_4_3_Core>
#include <QImage>
#include <QColor>

#include "miscellaneous.h"


// ========================================== Functions ===============================================//
std::string glErrorToString(GLenum err);

// ===================================== RandVecTex class =============================================//
class RandVecTex {
private:
    vec3f *m_tex;
    GLsizei m_size;
    GLfloat m_n; // parameter n controls the distribution
public:
    RandVecTex(GLsizei _size, GLfloat _n = 0);
    ~RandVecTex() {delete [] m_tex;}
    void setSize(GLsizei _size);
    void setN(GLsizei _n);
    void refresh();
    vec3f *tex() const {return m_tex;}
    GLsizei size() const {return m_size;}
    GLsizei numTexels() const {return m_size * m_size;}
    GLfloat texelSize() const {return 1.f / static_cast<GLfloat>(m_size);}
};

// ======================================= Texture class ==============================================//
class Texture {
private:
    QString m_filePath;
    GLfloat* m_tex;
    GLsizei m_width;
    GLsizei m_height;

    void setTexel(int x, int y, QColor c);
    // return the index of the red channel of texel (x, y) in m_tex;
    int idx(int x, int y) {return 3 * (x * m_width + y);}
public:
    Texture(const QString& _filePath);
    ~Texture() {delete [] m_tex;}
    GLfloat* tex() const {return m_tex;}
    GLsizei mipmapLevels() const;
    GLsizei width() const {return m_width;}
    GLsizei height() const {return m_height;}
    GLsizei numTexels() const {return m_width * m_height;}
    QString filePath() const {return m_filePath;}
    QString fileName() const {return shortName(m_filePath);}
};

// ==================================== ShaderProgram class ===========================================//
class Shader;
class ShaderProgram : protected QOpenGLFunctions_4_3_Core
{
public:
    ShaderProgram();
    ~ShaderProgram();
    GLuint shaderProg() {return m_shaderProg;}

    void attachShader(Shader* shader);
    void detachShader(Shader* shader);

    void detachAll();

    bool link();
    void useProgram() {glUseProgram(m_shaderProg);}

    void bindAttribLocation(GLuint index, const GLchar* name) {
        glBindAttribLocation(m_shaderProg, index, name);
    }

    GLint getAttribLocation(const GLchar* name) {
        return glGetAttribLocation(m_shaderProg, name);
    }

    GLuint getUniformLocation(const GLchar* name) {
        return glGetUniformLocation(m_shaderProg, name);
    }

    GLuint operator[] (const GLchar *uniform);

    void addUniform (const GLchar *uniform);
    void addUniforms (int numUniforms, const GLchar *uniforms[]);

private:
    GLuint m_shaderProg;
    std::vector<GLuint> m_shaders;
    std::map<std::string, GLuint> m_uniformLocs;
};


// =========================================== Shader class ===========================================//

class Shader : protected QOpenGLFunctions_4_3_Core
{
public:
    Shader(const char *fileName, GLenum shaderType);
    GLuint shaderObj() {return m_shaderObj;}
    ~Shader() {glDeleteShader(m_shaderObj);}

private:
    std::vector<std::string> readFile(const char *fileName);
    void compile();

    GLuint m_shaderObj;
};


#endif // GLHELPER_H
