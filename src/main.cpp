#include <QApplication>
#include <QSurfaceFormat>

#include "mainwindow.h"

class App : public QApplication
{
public:
    App(int argc, char **argv) : QApplication(argc, argv) {}
    bool notify(QObject * receiver, QEvent *e) {
        try {
            return QApplication::notify(receiver, e);
        } catch (Error e) {
            std::cout << "Error: " << e.error() << std::endl;
            return false;
         }
    }
};

int
main(int argc, char **argv)
{
    App app(argc, argv);

    QSurfaceFormat format;
    format.setDepthBufferSize(32);
    format.setStencilBufferSize(8);
    format.setVersion(4, 3);
    format.setProfile(QSurfaceFormat::CoreProfile);
    QSurfaceFormat::setDefaultFormat(format);

    try {
        MainWindow mwnd;
        mwnd.setWindowFlags(Qt::Window | Qt::FramelessWindowHint /*| Qt::WindowStaysOnTopHint*/);
        mwnd.resize(1920, 1080);
        mwnd.show();
        return app.exec();
    } catch (Error e) {
       std::cout << "Error: " << e.error() << std::endl;
    }
}
