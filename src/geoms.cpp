#include "geoms.h"

GLubyte
pickingID(GeomType _type)
{
    return static_cast<GLubyte>(_type);
}

// ========================================== Geom class ===============================================//
Geom::Geom(GeomType _type) : m_type(_type)
{
    m_pickColor = pickingID(m_type);
}

// ========================================== Pool class ==============================================//
Pool::Pool() : Geom(GeomType::POOL)
{
    m_edgeLength = 2.4f;
    m_height = 1.5f;
    m_texLength = 3.6f;
    m_shininess = 100.f;
    genMesh();
}

Pool::~Pool()
{
    delete [] m_positions;
    delete [] m_texCoords;
    delete [] m_normals;
}

void
Pool::genMesh()
{
    m_positions = new vec3f[numVertices()];
    m_texCoords = new vec2f[numVertices()];
    m_normals = new vec3f[numVertices()];
    GLfloat x = m_edgeLength / 2.f;
    GLfloat y = m_height / 2.f;
    // back
    m_positions[ 0].set(-x, -y, -x);
    m_positions[ 1].set( x, -y, -x);
    m_positions[ 2].set( x,  y, -x);

    m_positions[ 3].set(-x, -y, -x);
    m_positions[ 4].set( x,  y, -x);
    m_positions[ 5].set(-x,  y, -x);

    for (int i = 0; i < 6; ++i) {
        m_normals[i].set(0.f, 0.f, 1.f);
        m_texCoords[i].set((m_positions[i].x + x) / m_texLength,
                           (m_positions[i].y + y) / m_texLength);
    }

    // front
    m_positions[ 6].set( x,  y,  x);
    m_positions[ 7].set( x, -y,  x);
    m_positions[ 8].set(-x, -y,  x);

    m_positions[ 9].set( x,  y,  x);
    m_positions[10].set(-x, -y,  x);
    m_positions[11].set(-x,  y,  x);

    for (int i = 6; i < 12; ++i) {
        m_normals[i].set(0.f, 0.f, -1.f);
        m_texCoords[i].set((m_positions[i].x + x) / m_texLength,
                           (m_positions[i].y + y) / m_texLength);
    }

    // left
    m_positions[12].set(-x, -y, -x);
    m_positions[13].set(-x,  y, -x);
    m_positions[14].set(-x,  y,  x);

    m_positions[15].set(-x, -y, -x);
    m_positions[16].set(-x,  y,  x);
    m_positions[17].set(-x, -y,  x);

    for (int i = 12; i < 18; ++i) {
        m_normals[i].set(1.f, 0.f, 0.f);
        m_texCoords[i].set((m_positions[i].y + y) / m_texLength,
                           (m_positions[i].z + x) / m_texLength);
    }

    // right
    m_positions[18].set( x, -y,  x);
    m_positions[19].set( x,  y,  x);
    m_positions[20].set( x,  y, -x);

    m_positions[21].set( x, -y,  x);
    m_positions[22].set( x,  y, -x);
    m_positions[23].set( x, -y, -x);

    for (int i = 18; i < 24; ++i) {
        m_normals[i].set(-1.f, 0.f, 0.f);
        m_texCoords[i].set((m_positions[i].y + y) / m_texLength,
                           (m_positions[i].z + x) / m_texLength);
    }

    // bottom
    m_positions[24].set(-x, -y, -x);
    m_positions[25].set(-x, -y,  x);
    m_positions[26].set( x, -y,  x);

    m_positions[27].set(-x, -y, -x);
    m_positions[28].set( x, -y,  x);
    m_positions[29].set( x, -y, -x);
    for (int i = 24; i < 30; ++i) {
        m_normals[i].set(0.f, 1.f, 0.f);
        m_texCoords[i].set((m_positions[i].z + x) / m_texLength,
                           (m_positions[i].x + x) / m_texLength);
    }
}

// ========================================== Sphere class =============================================//
Sphere::Sphere(float _waterSurfaceHeight, float _poolEdgeLen, float _poolHeight) : Geom(GeomType::SPHERE)
{
    m_waterSurfaceHeight = _waterSurfaceHeight;
    m_poolEdgeLen = _poolEdgeLen;
    m_poolHeight = _poolHeight;
    m_radius = 0.3f;
    m_velocity = 0.f;
    m_density = 926.f;
    m_tessLevel = 16.f;
    m_translation = QVector3D(0.f, - m_poolHeight / 2.f + m_radius, 0.f);
    m_kd.set(1.f, 1.f, 1.f);
    updateAccelaeration();
    genMesh();
}

Sphere::~Sphere()
{
    delete [] m_positions;
    delete [] m_indices;
}

GLfloat
Sphere::radiusOnWaterSurface() const
{
    GLfloat absDeltaH = std::abs(m_waterSurfaceHeight - m_translation.y());
    GLfloat resultQuad = m_radius * m_radius - absDeltaH * absDeltaH;
    if (resultQuad <= 0) return 0.f;
    return static_cast<GLfloat>(std::sqrt(resultQuad));
}

GLfloat
Sphere::volumeUnderWater() const
{
    GLfloat deltaH = m_waterSurfaceHeight - m_translation.y() + m_radius;
    deltaH = clamp(deltaH, 0.f, 2 * m_radius);
    return M_PI * (m_radius * std::pow(deltaH, 2) - std::pow(deltaH, 3) / 3.f);
}

void Sphere::updateAccelaeration()
{
    GLfloat forceFromWater = volumeUnderWater() * WATER_DENSITY * G;
    GLfloat force = forceFromWater - mass() * G;
    m_acceleration = force / mass();
    if (std::abs(m_acceleration) < EPSILON_VELOCITY) m_acceleration = 0;
}

void
Sphere::moveInGravityMode(float deltaTime)
{
    // update position, if collision, change position to pool bottom height
    // update velocity, if at pool bottom and curr velocity < 0, reverse velocity direction
    // velocity is always multiplied with k (0 < k < 1) to simulate resistance
    // if velocity < EPSILON, velocity = 0
    GLfloat k = 0.995f;
    GLfloat deltaH = m_velocity * deltaTime;
    GLfloat poolBottomH = - m_poolHeight / 2;
    if (m_translation.y() + deltaH - m_radius < poolBottomH) {
        m_translation.setY(poolBottomH + m_radius);
    } else {
        m_translation.setY(m_translation.y() + deltaH);
    }
    if (m_translation.y() - m_radius == poolBottomH && m_velocity < 0) {
        m_velocity *= -1;
    } else {
        m_velocity += deltaTime * m_acceleration;
    }
    m_velocity *= k;
    if (m_acceleration == 0 && std::abs(m_velocity) < EPSILON_VELOCITY) {
        m_velocity = 0;
    }
    // std::cout << "v = " << m_velocity << ", a = " << m_acceleration << std::endl;
    updateAccelaeration();

}

void
Sphere::moveInUserManipMode(const QVector3D &deltaTWorld)
{
    m_velocity = 0;
    m_translation += deltaTWorld;
    GLfloat poolBottomH = - m_poolHeight / 2 + m_radius;
    GLfloat deltaMax = m_poolEdgeLen / 2.f - m_radius;
    m_translation.setX(clamp(m_translation.x(), -deltaMax, deltaMax));
    m_translation.setZ(clamp(m_translation.z(), -deltaMax, deltaMax));
    m_translation.setY(clamp(m_translation.y(), poolBottomH, m_translation.y()));
}

void
Sphere::genMesh()
{
    m_positions = new vec3f[numVertices()];
    m_positions[0].set( 0.f,  m_radius,  0.f);
    m_positions[1].set(-m_radius,  0.f,  0.f);
    m_positions[2].set( 0.f,  0.f,  m_radius);
    m_positions[3].set( 0.f, -m_radius,  0.f);
    m_positions[4].set( m_radius,  0.f,  0.f);
    m_positions[5].set( 0.f,  0.f, -m_radius);

    GLubyte idx[24] = {0, 1, 2, // top left front
                       0, 4, 5, // top right back
                       3, 2, 1, // bottom left front
                       5, 4, 3, // bottom right back
                       0, 2, 4, // top right front
                       5, 1, 0, // top left back
                       2, 3, 4, // bottom right front
                       1, 5, 3};// bottom left back
    m_indices = new GLubyte[numIndices()];
    for (int i = 0; i < numIndices(); ++i) {
        m_indices[i] = idx[i];
    }
}

// ======================================= Heightmap class ============================================//
Heightmap::~Heightmap()
{
    delete [] m_iPositions;
    delete [] m_indices;
}

GLuint
Heightmap::idxAt(int s, int t) const
{
    return static_cast<GLuint>(t * m_size + s);
}

void
Heightmap::genIPositions()
{
    m_iPositions = new vec2i[numVertices()];
    for (int t = 0; t < m_size; ++t) {
        for (int s = 0; s < m_size; ++s) {
            m_iPositions[idxAt(s, t)].set(s, t);
        }
    }
}

void
Heightmap::genIndices()
{
    m_indices = new GLuint[numIndices()];
    int i = 0;
    for (int t = 0; t < m_size - 1; ++t) {
        for (int s = 0; s < m_size - 1; ++s) {
            /*
            m_indices[i + 0] = idxAt(s + 0, t + 0);
            m_indices[i + 1] = idxAt(s + 0, t + 1);
            m_indices[i + 2] = idxAt(s + 1, t + 0);
            m_indices[i + 3] = idxAt(s + 1, t + 0);
            m_indices[i + 4] = idxAt(s + 0, t + 1);
            m_indices[i + 5] = idxAt(s + 1, t + 1);
            */
            m_indices[i + 0] = idxAt(s + 0, t + 0);
            m_indices[i + 1] = idxAt(s + 1, t + 0);
            m_indices[i + 2] = idxAt(s + 0, t + 1);
            m_indices[i + 3] = idxAt(s + 1, t + 0);
            m_indices[i + 4] = idxAt(s + 1, t + 1);
            m_indices[i + 5] = idxAt(s + 0, t + 1);
            i += 6;
        }
    }
}

void
Heightmap::genMesh()
{
    genIPositions();
    genIndices();
}

// ====================================== WaterSurface class ==========================================//
WaterSurface::WaterSurface(int _numQuadsPerEdge, int _hmapSize) : Geom(GeomType::WATER_SURFACE)
{
    m_height = 0.45f;
    m_edgeLength = 2.4f;
    m_LoDFactor =  3000.f;
    m_numQuadsPerEdge = _numQuadsPerEdge;
    m_hmapSize = _hmapSize;
    m_indices = nullptr;
    m_positions = nullptr;
    m_texCoords = nullptr;
    genMesh();
}

WaterSurface::~WaterSurface()
{
    delete [] m_positions;
    delete [] m_indices;
    delete [] m_texCoords;
}

void
WaterSurface::genMesh()
{
    genPositions();
    genTexCoords();
    genIndices();
}

void
WaterSurface::genPositions()
{
    if (m_positions != nullptr) delete [] m_positions;
    m_positions = new vec2f[numVertices()];
    GLfloat quadEdgeLen = m_edgeLength / m_numQuadsPerEdge;
    int halfNumQuads = m_numQuadsPerEdge / 2;
    for (int z = -halfNumQuads; z <= halfNumQuads; ++z) {
        for (int x = -halfNumQuads; x <= halfNumQuads; ++x) {
            m_positions[idxAt(z, x)].set(x * quadEdgeLen, z * quadEdgeLen);
        }
    }
}

void
WaterSurface::genTexCoords()
{
    if (m_texCoords != nullptr) delete [] m_texCoords;
    m_texCoords = new vec2f[numVertices()];
    int halfNumQuads = m_numQuadsPerEdge / 2;
    GLfloat quadProp = 1.f / static_cast<GLfloat>(m_numQuadsPerEdge);
    for (int z = -halfNumQuads; z <= halfNumQuads; ++z) {
        for (int x = -halfNumQuads; x <= halfNumQuads; ++x) {
            m_texCoords[idxAt(-z, x)].set(0.5 + x * quadProp, 0.5 + z * quadProp);
        }
    }
}

void
WaterSurface::genIndices()
{
    if (m_indices != nullptr) delete [] m_indices;
    m_indices = new GLuint[numIndices()];
    int halfNumQuads = m_numQuadsPerEdge / 2;
    int i = 0;
    for (int z = -halfNumQuads; z < halfNumQuads; ++z) {
        for (int x = -halfNumQuads; x < halfNumQuads; ++x) {
            m_indices[i]     = idxAt(z    , x);
            m_indices[i + 1] = idxAt(z + 1, x);
            m_indices[i + 2] = idxAt(z,     x + 1);
            m_indices[i + 3] = idxAt(z + 1, x + 1);
            i += 4;
        }
    }
}

GLint
WaterSurface::maxTessLevel() const
{
    GLint logNumTexelsPerQuadEdge = static_cast<GLint>(std::log2(numTexelsPerQuadEdge()));
    return static_cast<GLint>(std::pow(2, logNumTexelsPerQuadEdge));
}

vec2i
WaterSurface::texelIPos(const vec3f &worldPos) const
{
    GLfloat x = (worldPos.x + m_edgeLength / 2.f) / m_edgeLength;
    GLfloat z = (worldPos.z + m_edgeLength / 2.f) / m_edgeLength;
    // x = clamp(x, 0.f, 1.f);
    // z = clamp(z, 0.f, 1.f);
    z = 1 - z;
    GLint s = static_cast<GLint>(std::floor(x * m_hmapSize));
    GLint t = static_cast<GLint>(std::floor(z * m_hmapSize));
    return vec2i(s, t);
}

vec2i
WaterSurface::texelIPos(const QVector3D &worldPos) const
{
    GLfloat x = (worldPos.x() + m_edgeLength / 2.f) / m_edgeLength;
    GLfloat z = (worldPos.z() + m_edgeLength / 2.f) / m_edgeLength;
    // x = clamp(x, 0.f, 1.f);
    // z = clamp(z, 0.f, 1.f);
    z = 1 - z;
    GLint s = static_cast<GLint>(std::floor(x * m_hmapSize));
    GLint t = static_cast<GLint>(std::floor(z * m_hmapSize));
    return vec2i(s, t);
}

GLfloat
WaterSurface::texelDist(GLfloat worldDist) const
{
    return worldDist * static_cast<GLfloat>(m_hmapSize) / m_edgeLength;
}

GLfloat
WaterSurface::world2TexelFactor() const
{
    return static_cast<GLfloat>(m_hmapSize) / m_edgeLength;
}

int
WaterSurface::idxAt(int z, int x) const
{
    int halfNumQuads = m_numQuadsPerEdge / 2;
    assert(std::abs(z) <= halfNumQuads);
    assert(std::abs(x) <= halfNumQuads);

    return (halfNumQuads + z) * (m_numQuadsPerEdge + 1) + (halfNumQuads + x);
}

// ====================================== ScreenQuad class ==========================================//
ScreenQuad::ScreenQuad()
{
    m_positions[0].set(-1.f, -1.f); m_texCoords[0].set(0.f, 0.f);
    m_positions[1].set( 1.f, -1.f); m_texCoords[1].set(1.f, 0.f);
    m_positions[2].set( 1.f,  1.f); m_texCoords[2].set(1.f, 1.f);
    m_positions[3].set(-1.f,  1.f); m_texCoords[3].set(0.f, 1.f);
}
