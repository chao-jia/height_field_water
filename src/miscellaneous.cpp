#include "miscellaneous.h"

std::mt19937 GEN{};

// ========================================= some Structs ===========================================//
void
vec2i::set(GLint _s, GLint _t)
{
    s = _s;
    t = _t;
}

void
vec2f::set(GLfloat _x, GLfloat _y)
{
    x = _x;
    y = _y;
}

void
vec3f::set(GLfloat _x, GLfloat _y, GLfloat _z)
{
    x = _x;
    y = _y;
    z = _z;
}

vec3f
vec3f::xMirror() const
{
    return vec3f(-x, y, z);
}

vec3f
vec3f::yMirror() const
{
    return vec3f(x, -y, z);
}

vec3f
vec3f::zMirror() const
{
    return vec3f(x, y, -z);
}

vec3f
vec3f::mirror(char axis) const
{
    switch (axis) {
    case 'X':
        return xMirror();
    case 'Y':
        return yMirror();
    case 'Z':
        return zMirror();
    default:
        std::cout << "misc::vec3f::mirror->unkown axis" << std::endl;
        return vec3f();
    }
}

vec2i operator +(const vec2i &v1, const vec2i &v2)
{
    return vec2i(v1.s + v2.s, v1.t + v2.t);
}

vec2f operator +(const vec2f &v1, const vec2f &v2)
{
    return vec2f(v1.x + v2.x, v1.y + v2.y);
}

vec3f operator +(const vec3f &v1, const vec3f &v2)
{
    return vec3f(v1.x + v2.x, v1.y + v2.y, v1.z + v2.z);
}

// ========================================= Functions ===============================================//
void
getMat4x4Data(const QMatrix4x4 &mat, GLfloat matData[])
{
    float matDataf[16];
    // row major
    mat.copyDataTo(matDataf);
    for (int i = 0; i < 16; ++i) {
        matData[i] = static_cast<GLfloat>(matDataf[i]);
    }
}

void
getMat3x3Data(const QMatrix3x3 &mat, GLfloat matData[])
{
    float matDataf[9];
    //row major
    mat.copyDataTo(matDataf);
    for (int i = 0; i < 9; ++i) {
        matData[i] = static_cast<GLfloat>(matDataf[i]);
    }
}

void
getVec3DData(const QVector3D &vec, GLfloat vecData[])
{
    vecData[0] = static_cast<GLfloat>(vec.x());
    vecData[1] = static_cast<GLfloat>(vec.y());
    vecData[2] = static_cast<GLfloat>(vec.z());
}

void
get3fvData(const vec3f &vec, GLfloat vecData[])
{
    vecData[0] = vec.x;
    vecData[1] = vec.y;
    vecData[2] = vec.z;
}

void
printMat4x4(const QMatrix4x4 &mat, const char matName[])
{
    float matDataf[16];
    mat.copyDataTo(matDataf);

    std::cout << matName << ":\n";
    for (int i = 0; i < 16; ++i) {
        std::cout << matDataf[i] << " ";
        if (i % 4 == 3) std::cout << std::endl;
    }
    std::cout << std::endl;
}

void
printMat3x3(const QMatrix3x3 &mat, const char matName[])
{
    float matDataf[9];
    mat.copyDataTo(matDataf);

    std::cout << matName << ":\n";
    for (int i = 0; i < 9; ++i) {
        std::cout << matDataf[i] << " ";
        if (i % 3 == 2) std::cout << std::endl;
    }
}

void
printVector3D(const QVector3D &pos, const char vecName[])
{
    std::cout << vecName << " = ("
              << pos.x() << ", "
              << pos.y() << ", "
              << pos.z() << ")"
              << std::endl;
}

std::ostream& operator<<(std::ostream &os, const QString &qstr)
{
    os << qstr.toStdString() << std::flush;
    return os;
}

std::ostream& operator<<(std::ostream& os, const QVector3D& qvec3d)
{
    os << "(" << qvec3d.x() << ", " << qvec3d.y() << ", " << qvec3d.z() << ")" << std::flush;
    return os;
}

QString
shortName(const QString& filePath)
{
    int lastSlashIdx = filePath.lastIndexOf('/');
    return filePath.right(filePath.length() - lastSlashIdx - 1);
}

GLfloat
randf(GLfloat min, GLfloat max)
{
    std::uniform_real_distribution<> dis(min, max);
    return static_cast<GLfloat> (dis(GEN));
}

int
randi(int min, int max)
{
     std::uniform_int_distribution<> dis(min, max);
     return dis(GEN);
}

vec3f
rand3f(GLfloat min, GLfloat max)
{
    vec3f r3fv;
    r3fv.set(randf(min, max), randf(min, max), randf(min, max));
    return r3fv;
}

// random directions around direction (0, 0, 1)
void
randDirs(int numDirs, vec3f dirs[], float n)
{
    for (int i = 0; i < numDirs; ++i) {
        GLfloat xi1 = randf(0.f, 1.f);
        GLfloat xi2 = randf(0.f, 1.f);
        GLfloat theta = std::acos(std::pow(1 - xi1, 1.f / (n + 1)));
        GLfloat phi = 2 * M_PI * xi2;
        dirs[i].x = std::sin(theta) * std::cos(phi);
        dirs[i].y = std::sin(theta) * std::sin(phi);
        dirs[i].z = std::cos(theta);
    }
}

// ======================================= Trackball class ===========================================//
QVector3D
Trackball::projOnSphere(const QPointF &pos)
{
    float z;
    QVector2D segment = QVector2D(pos - m_center);

    if (segment.lengthSquared() > m_radius * m_radius / 2.f) {
        z = m_radius * m_radius / (2.f * segment.length());
    }
    else {
        z = std::sqrt(m_radius * m_radius - segment.lengthSquared());
    }
    return QVector3D(segment, z);
}

Trackball::Trackball(const QPointF &rotationCenter,
                     const QPointF &lastPos, const QPointF &currentPos)
{
    m_noRotation = false;
    m_center = rotationCenter;
    m_radius = 1.5f;
    float VERY_SMALL = 0.00001f;
    if (currentPos.x() > lastPos.x() + VERY_SMALL) m_isInPostiveDir = true;
    else m_isInPostiveDir = false;

    QVector3D lastVec = projOnSphere(lastPos);
    QVector3D currentVec = projOnSphere(currentPos);
    float dotProduct = QVector3D::dotProduct(lastVec, currentVec);

    if (dotProduct == 0.f) {
        m_noRotation = true;
    } else {
        float cosine = dotProduct / (lastVec.length() * currentVec.length());
        cosine = clamp(cosine, -1.f, 1.f);
        m_angle = 5 * std::acos(cosine) * 180.f / M_PI;
        m_axis = QVector3D::crossProduct(lastVec, currentVec);
        m_axis.normalize();
    }
}

QQuaternion
Trackball::quaternion(const QMatrix4x4 &viewMat, bool optEnabled)
{
    if (m_noRotation) return QQuaternion{};
    float cosAngleZ = QVector3D::dotProduct(m_axis, QVector3D(0.f, 1.f, 0.f));
    QVector3D axisWorld = QVector3D(viewMat.inverted() * QVector4D(m_axis, 0.f));
    axisWorld.normalize();
    if (optEnabled) {
        if (std::abs(cosAngleZ) > 0.7) {
            if (axisWorld.y() == 0.f) return QQuaternion{};
            axisWorld = QVector3D(0.f, axisWorld.y()/std::abs(axisWorld.y()), 0.f);
        } else if (std::abs(cosAngleZ) <= 0.3) {
            if (axisWorld.x() == 0.f && axisWorld.z() == 0.f) {
                return QQuaternion{};
            }
            axisWorld = QVector3D(axisWorld.x(), 0.f, axisWorld.z());
        }
        axisWorld.normalize();
    }
    return QQuaternion::fromAxisAndAngle(axisWorld, m_angle);
}

QQuaternion
Trackball::quaternion(const QVector3D& axis)
{
    if (m_noRotation) {
        return QQuaternion{};
    }
    if (m_isInPostiveDir) {
        return QQuaternion::fromAxisAndAngle(axis, m_angle);
    } else {
        return QQuaternion::fromAxisAndAngle(-axis, m_angle);
    }

}

