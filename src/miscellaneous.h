#ifndef MISCELLANEOUS_H
#define MISCELLANEOUS_H

#include <iostream>
#include <cmath>
#include <random>
#include <chrono>
#include <cassert>
#include <limits>

#include <QString>
#include <QOpenGLFunctions_4_3_Core>

#include <QMatrix4x4>
#include <QMatrix3x3>
#include <QVector4D>
#include <QVector3D>
#include <QVector2D>
#include <QQuaternion>
#include <QPointF>
#include <QPoint>

#include "templates.h"

#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif

extern std::mt19937 GEN;
// ======================================= some Structs ===========================================//
struct vec2i {
    GLint s;
    GLint t;
    vec2i () : s(0), t(0) {}
    vec2i (GLint _s, GLint _t) : s(_s), t(_t) {}
    void set(GLint _s, GLint _t);
    void print() const {
        std::cout << "(" << s << ", " << t << ")" << std::flush;
    }
};

struct vec2f {
    GLfloat x;
    GLfloat y;
    vec2f() {}
    vec2f(GLfloat _x, GLfloat _y) : x(_x), y(_y) {}
    void set(GLfloat _x, GLfloat _y);
    void print() const {
        std::cout << "(" << x << ", " << y << ")";
    }
};

struct vec3f {
    GLfloat x;
    GLfloat y;
    GLfloat z;
    vec3f() {}
    vec3f(GLfloat _x, GLfloat _y, GLfloat _z) : x(_x), y(_y), z(_z) {}
    void set(GLfloat _x, GLfloat _y, GLfloat _z);

    // reflection about yz-plane
    vec3f xMirror() const;
    // reflection about xz-plane
    vec3f yMirror() const;
    // reflection about xy-plane
    vec3f zMirror() const;
    // reflection about the plane with normal 'axis'
    vec3f mirror(char axis) const;

    void print() const {
        std::cout << "(" << x << ", " << y << ", " << z << ")";
    }
};

vec2i operator +(const vec2i &v1, const vec2i &v2);
vec2f operator +(const vec2f &v1, const vec2f &v2);
vec3f operator +(const vec3f &v1, const vec3f &v2);

// ========================================= Functions ===============================================//
// put raw data of a matrix in array matData[], row major
void getMat4x4Data(const QMatrix4x4 &mat, GLfloat matData[]);
void getMat3x3Data(const QMatrix3x3 &mat, GLfloat matData[]);
// put raw data of a vector in array vecData[]
void getVec3DData(const QVector3D &vec, GLfloat vecData[]);
void get3fvData(const vec3f &vec, GLfloat vecData[]);

std::ostream& operator<<(std::ostream&, const QString&);
std::ostream& operator<<(std::ostream&, const QVector3D&);

void printMat4x4(const QMatrix4x4 &mat, const char matName[] = "Matrix4x4");
void printMat3x3(const QMatrix3x3 &mat, const char matName[] = "Matrix3x3");
void printVector3D(const QVector3D &pos, const char vecName[] = "Vector3D");
QString shortName(const QString& filePath);

GLfloat randf(GLfloat min, GLfloat max);
int randi(int min, int max);
vec3f rand3f(GLfloat min, GLfloat max);

// random directions in tangent space, Normal = (0, 0, 1)
void randDirs(int numDirs, vec3f dirs[], float n = 0); // parameter n controls the distribution
// ======================================= Trackball class ===========================================//
class Trackball {
private:
    QQuaternion m_quaternion;
    float m_radius;
    QPointF m_center;
    QVector3D m_axis;
    float m_angle;
    bool m_noRotation;
    bool m_isInPostiveDir;

    QVector3D projOnSphere(const QPointF& pos);
public:
    Trackball(const QPointF& rotationCenter,
              const QPointF& lastPos, const QPointF& currentPos);
    // return the quaternion with rotation axis in World space
    QQuaternion quaternion(const QMatrix4x4 &viewMat, bool optEnabled = false);
    // change rotation axis
    QQuaternion quaternion(const QVector3D& axis);
};

// ======================================== Error class ==============================================//

class Error {
    QString m_error;
public:
    Error (QString msg) {
        m_error = msg;
    }
    Error (std::string msg) {
        m_error = QString(msg.c_str());
    }

    QString error() const {
        return m_error;
    }
};

#endif // MISCELLANEOUS_H
