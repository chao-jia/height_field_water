#ifndef GLWIDGET_H
#define GLWIDGET_H
#include <QMouseEvent>
#include <QTimer>
#include <QElapsedTimer>
#include <QOpenGLWidget>
#include <QOpenGLFramebufferObject>

#include "glhelper.h"
#include "camera.h"
#include "geoms.h"

const int SCENE      = 0;
const int HM_NOR_TEX = 1;
const int HM_POS_TEX = 2;
const int HM_VEL_TEX = 3;
const int CAUSTICS_TEX = 4;
const int HM_MESH_TEST = 5;
const int SHADOW_MAP_TEX = 6;

#ifndef FLT_MAX
    const GLfloat FLT_MAX = std::numeric_limits<float>::max();
#endif

class GLWidget : public QOpenGLWidget, protected QOpenGLFunctions_4_3_Core
{
    Q_OBJECT
private:
    Pool *m_pool;
    GLuint m_poolVao;
    GLuint m_poolBos[3]; // pos, normal, tex coord
    ShaderProgram *m_poolProg;
    GLuint m_tileTex;
    GLint m_tileTexSize;

    Sphere * m_sphere;
    GLuint m_sphereVao;
    GLuint m_sphereBos[2]; // pos, index
    ShaderProgram *m_sphereProg;

    Heightmap *m_hmap;
    GLuint m_hmapVao;
    GLuint m_hmapBos[2]; // pos, index
    ShaderProgram *m_hmapDiffuseProg;// update position and velocity for every texel of the heightmap
    ShaderProgram *m_hmapNormalProg; // calculate normal at every texel of the heightmap
    ShaderProgram *m_hmapPosRenderProg; // render heightmap on the screen for testing
    ShaderProgram *m_hmapNormalRenderProg; // render normal texture on the screen
    ShaderProgram *m_causticsCalcProg;
    GLfloat *m_initTexPos;
    GLfloat *m_initTexVel;
    // ping-pong buffering
    GLuint m_hmapDiffuseFBO;
    GLuint m_hmapPosTex[2];
    GLuint m_hmapVelocityTex[2];
    GLuint m_causticsTex;
    GLuint m_hmapNormalTex;
    GLuint m_hmapDepthTex;
    int m_writeTexIdx; // 0 or 1
    int m_hmapSize;

    WaterSurface *m_waterSurface;
    GLuint m_waterSurfaceVao;
    GLuint m_waterSurfaceBos[3]; // pos, index, tex coord
    ShaderProgram *m_waterSurfaceProg; // CULL_FACE off
    int m_wsNumQuadsPerEdge;

    GLuint m_screenQuadVao;
    GLuint m_screenQuadBos[2];
    ShaderProgram *m_screenQuadProg;

    GLuint m_pickingFBO;
    GLuint m_pickingTexes[5]; // color, picking color, world position, normal, depth
    GLuint m_pickingPpBO; // Pixel pack Buffer Object
    GLuint m_worldPosPpBo;
    GLuint m_normalPpBo;

    GLuint m_shadowMapFBO;
    GLuint m_shadowMapTex; // depth texture
    int m_shadowMapSize;
    ShaderProgram *m_drawShadowMapTexProg;

    QVector3D m_sphereLastPos; // position of the sphere in world space during last updateScene()
    QPoint m_mouseLastPos; // position of mouse (bottom left is the origin of the window)
    QVector3D m_rippleWorldPos;
    GLubyte m_pickingID; // pickingID(GeomType)

    QVector3D m_lightPos;
    QVector3D m_lightDir;
    GLfloat m_DistlightPos2Plane; //used for collision test between light and ray
    GLfloat m_lightPosPlane; // light position always on this plane
    QVector3D m_ILMax;
    GLfloat m_sExp; // spotlight's exponent property
    GLfloat m_penumbraAngle; // angle at which its intensity starts to decrease
    GLfloat m_umbraAngle; // angle at which the intensity reaches 0 (m_penumbraAngle < m_umbraAngle)

    Cam *m_cam;
    PerspectiveProj *m_persp;

    bool m_wireFrameOn;
    bool m_inGravityMode;
    bool m_paused;
    QTimer *m_timer; // sphere move in gravity mode, wave generation /propagation, heightmap update
    int m_contentIdx; // SCENE, HM_POS_TEX, HM_VEL_TEX, or HM_NOR_TEX

    GLint m_numSampledRays; // number of sampled rays during distributed ray tracing
    GLint m_recursionDepth; // ray tracing depth

    int INTERVAL; // refresh interval in milliseconds
    int MAX_FPS_COUNT;
    int m_fpsCounter;
    int m_sumPaintTime; // milliseconds
    QElapsedTimer m_fpsTimer;

    void setupLightDir();
    void setupPickingFBO();
    void setupShadowMapFBO();
    void setupPool();
    void setupSphere();
    void setupHeightmap();
    void setupWaterSurface();
    void setupScreenQuad();
    void setupSmallViewport();

    void genShaderProg();
    void delShaderProg(ShaderProgram *);
    void delAllShaderProgs();

    void diffuseHmap();
    void calcHmapNormal();
    void calcCaustics();

    void genShadowMap();
    void drawShadowMapTex();

    void drawHmapPositionTex();
    void drawHmapVelocityTex();
    void drawHmapNormalTex();
    void testHmapMesh();
    void drawCausticsTex();

    void drawPool(bool inLightSpace = false);
    void drawSphere(bool inLightSpace = false);
    void drawWaterSurface();

    void setupScreenUniforms();
    void drawScene();

    GLubyte pick(const QPoint &pos2D);
    void locateRipple(const QPoint &pos2D);
    void locationNormal(const QPoint &pos2D);
    void drawScreenQuad();

    QMatrix4x4 VP(bool inLightSpace = false);
    void catchGLError(const QString& segmntName);
public:
    GLWidget(QWidget *parent = 0);
    ~GLWidget();
    QPoint toGLWndCoord(const QPoint &p) const;
    QPointF toNDC(const QPoint &p) const; // // p already in GLWndCoord
protected:
    void initializeGL() override;
    void resizeGL(int w, int h) override;
    void paintGL() override;
    void keyPressEvent(QKeyEvent *event) override;
    void mousePressEvent(QMouseEvent *) override;
    void mouseReleaseEvent(QMouseEvent *event) override;
    void mouseMoveEvent(QMouseEvent *) override;
    void releaseGL();
public slots:
    void refreshShaderProg();
    // void reset();
    void setWireframeMode() {m_wireFrameOn = !m_wireFrameOn;}
    void setDrawContent(int idx) {m_contentIdx = idx;}

    // update sphere position
    // wave from sphere movement
    // calc wave (ripples, sphere movement),
    // diffuse heightmap, calc heightmap normal, then call update
    void updateScene();
signals:
    void FPSChanged(float fps);
};

#endif // GLWIDGET_H
