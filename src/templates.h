#ifndef TEMPLATE_H
#define TEMPLATE_H

template<typename T>
T
clamp(T x, T min, T max)
{
    if (x < min) x = min;
    else if (x > max) x = max;
    return x;
}

#endif // TEMPLATE_H
