#include "camera.h"

// ====================================== PerspectiveProj class ==================================== //
PerspectiveProj::PerspectiveProj(int width, int height)
{
    setAspectRatio(width, height);
    reset();
}

PerspectiveProj::PerspectiveProj(float verticalAngle, float aspectRatio,
                                 float nearPlane, float farPlane)
{
    m_verticalAngle = verticalAngle;
    m_aspectRatio = aspectRatio;
    m_nearPlane = nearPlane;
    m_farPlane = farPlane;
}

PerspectiveProj::PerspectiveProj(float verticalAngle, int width, int height,
                                 float nearPlane, float farPlane)
{
    m_verticalAngle = verticalAngle;
    setAspectRatio(width, height);
    m_nearPlane = nearPlane;
    m_farPlane = farPlane;
}

void
PerspectiveProj::setAspectRatio(int width, int height)
{
    m_aspectRatio = static_cast<float>(width) / static_cast<float>(height);
}

void
PerspectiveProj::zoom(float delta)
{
    m_verticalAngle -= delta;
    m_verticalAngle = clamp(m_verticalAngle, 15.f, 150.f);
}

void
PerspectiveProj::reset()
{
    m_verticalAngle = 45;
    m_nearPlane = 0.01f;
    m_farPlane = 100.f;
}

QMatrix4x4
PerspectiveProj::projMat() const
{
    QMatrix4x4 projMatrix{};
    projMatrix.perspective(m_verticalAngle, m_aspectRatio,
                           m_nearPlane, m_farPlane);
    return projMatrix;
}

// ========================================= Cam class ============================================ //
Cam::Cam(const QVector3D &_initPoi, const QVector3D &_initPos)
{
    m_initPoi = _initPoi;
    m_initPos = _initPos;
    reset();
}

Cam::Cam()
{
    m_initPoi =  QVector3D(0.f, 0.f, 0.f);
    // m_initPos = QVector3D(-1.f, 2.5f, 4.0f);
    m_initPos = QVector3D(0.f, 4.5f, 0.f);
    reset();
}

void
Cam::reset()
{
    QVector3D lookDir = m_initPoi - m_initPos;
    lookDir.setY(0.f);
    if (lookDir.length() == 0.f) {
        m_initPos += QVector3D(0.001f, 0.f, 0.f);
    }
    m_poi = m_initPoi;
    m_pos = m_initPos;
    m_up = QVector3D(0.f, 1.f, 0.f);
}

QVector3D
Cam::w() const
{
    QVector3D _w = - (m_poi - m_pos);
    return _w.normalized();
}

QVector3D
Cam::u() const
{
    QVector3D _u = QVector3D::crossProduct(m_up, w());
    return _u.normalized();
}

QVector3D
Cam::v() const
{
    QVector3D _v = QVector3D::crossProduct(w(), u());
    return _v.normalized();
}

QMatrix4x4
Cam::viewMat() const
{
    QMatrix4x4 viewMatrix{};
    float m14 = - QVector3D::dotProduct(u(), eye());
    float m24 = - QVector3D::dotProduct(v(), eye());
    float m34 = - QVector3D::dotProduct(w(), eye());
    viewMatrix.setRow(0, QVector4D(u(), m14));
    viewMatrix.setRow(1, QVector4D(v(), m24));
    viewMatrix.setRow(2, QVector4D(w(), m34));
    return viewMatrix;
}

QMatrix4x4
Cam::invertedViewMat() const
{
    return viewMat().inverted();
}

void
Cam::orbit(QPointF rotationCenter, QPointF lastPos, QPointF currentPos)
{
    Trackball trackball(rotationCenter, currentPos, lastPos);
    QQuaternion rotation = trackball.quaternion(viewMat(), false);
    QVector3D eyeInPoi = m_pos - m_poi;
    QVector3D rotatedEyeInPoi = rotation.rotatedVector(eyeInPoi);
    QVector3D eyeInWorld = rotatedEyeInPoi + m_poi;
    QVector3D lookDirOld = m_poi - m_pos;
    lookDirOld.setY(0.f);
    lookDirOld.normalize();
    QVector3D lookDirNew = m_poi - eyeInWorld;
    lookDirNew.setY(0.f);
    if (lookDirNew.length() == 0.f) return;
    lookDirNew.normalize();
    if (QVector3D::dotProduct(lookDirOld, lookDirNew) < 0.f) {
        return;
    }
    m_pos = eyeInWorld;
}

