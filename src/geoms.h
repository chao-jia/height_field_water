#ifndef GEOMS_H
#define GEOMS_H

#include "miscellaneous.h"
#include "templates.h"
#include "glhelper.h"

enum class GeomType {UNKNOWN = 0, POOL, SPHERE, WATER_SURFACE};
const float G = 9.8f;
const float WATER_DENSITY = 1000.f;
const float EPSILON_VELOCITY = 0.05f;
const float EPSILON_DISTANCE = 0.025f;

GLubyte pickingID(GeomType _type);

// ========================================== Geom class ===============================================//
class Geom
{
protected:
    GeomType m_type;
    GLubyte m_pickColor; // red channel
public:
   Geom(GeomType _type);
   virtual ~Geom() {}
    GeomType type() const {return m_type;}
    GLubyte pickColor() const {return m_pickColor;}
};

// ========================================== Pool class ==============================================//
/* pool: 2.4m x 2.4m x 2.4m
 * texture: 24 x 24 grid (every cell: 0.1m x 0.1m)
 */

class Pool : public Geom
{
private:
    GLfloat m_edgeLength;
    GLfloat m_height;
    GLfloat m_texLength; // length of the texture in world space
    GLfloat m_shininess;

    vec3f *m_positions;
    vec2f *m_texCoords;
    vec3f *m_normals;

    void genMesh();
public:
    Pool();
    ~Pool();
    GLfloat edgeLength() const {return m_edgeLength;}
    GLfloat height() const {return m_height;}
    GLfloat texLength() const {return m_texLength;}
    GLsizei numVertices() const {return 30;}

    GLfloat shininess() const {return m_shininess;}

    const void *positions() const {return m_positions;}
    const void *texCoords() const {return m_texCoords;}
    const void *normals() const {return m_normals;}

    GLint lenOfPosition() const {return 3;}
    GLint lenOfTexCoord() const {return 2;}
    GLint lenOfNormal() const {return 3;}

    GLsizei sizeOfPositions() const {return numVertices() * sizeof(vec3f);}
    GLsizei sizeOfTexCoords() const {return numVertices() * sizeof(vec2f);}
    GLsizei sizeOfNormals() const {return numVertices() * sizeof(vec3f);}

};

// ========================================== Sphere class =============================================//
class Sphere : public Geom
{
private:
    GLfloat m_waterSurfaceHeight;
    GLfloat m_poolEdgeLen;
    GLfloat m_poolHeight;

    GLfloat m_radius;
    GLfloat m_density;
    GLfloat m_acceleration;
    GLfloat m_velocity;
    QVector3D m_translation;
    GLfloat m_tessLevel;

    vec3f m_kd;

    vec3f *m_positions;
    GLubyte *m_indices;

    void genMesh();
public:
    Sphere(float _waterSurfaceHeight, float _poolEdgeLen, float _poolHeight);
    ~Sphere();

    GLfloat radius() const {return m_radius;}
    GLfloat velocity() const {return m_velocity;}
    GLfloat acceleration() const {return m_acceleration;}
    GLfloat volume() const {return 4.f * M_PI * std::pow(m_radius, 3) / 3.f;}
    GLfloat mass() const {return volume() * m_density;}
    GLfloat density() const {return m_density;}
    GLfloat tessLevel() const {return m_tessLevel;}
    QVector3D translation() const {return m_translation;}

    vec3f kd() const {return m_kd;}

    GLfloat radiusOnWaterSurface() const;
    GLfloat volumeUnderWater() const;
    void updateAccelaeration();
    void setVelocity(GLfloat v) {m_velocity = v;}
    void moveInGravityMode(float deltaTime);
    void moveInUserManipMode(const QVector3D& deltaTWorld);

    GLsizei numVertices() const {return 6;}
    GLint lenOfPosition() const {return 3;}
    GLsizei sizeOfPositions() const {return numVertices() * sizeof(vec3f);}
    const void* positions() const {return m_positions;}

    GLsizei numIndices() const {return 24;}
    GLsizei sizeOfIndices() const {return numIndices() * sizeof(GLubyte);}
    const void* indices() const {return m_indices;}
};

// ======================================= Heightmap class ============================================//
class Heightmap
{
private:
    int m_size; // size x size heightmap
    GLuint *m_indices;
    vec2i *m_iPositions;

    void genMesh();
    void genIPositions();
    void genIndices();
public:
    Heightmap(int _size) : m_size(_size) {genMesh();}
    ~Heightmap();

    int size() const {return m_size;}
    GLuint idxAt(int s, int t) const;
    GLsizei numVertices() const {return static_cast<GLsizei>(std::pow(m_size, 2));}
    GLint lenOfIPosition() const {return 2;}
    GLsizei sizeOfIPositions() const {return numVertices() * sizeof(vec2i);}
    const void* iPositions() const {return m_iPositions;}

    GLsizei numIndices() const {return static_cast<GLsizei>(6 * (m_size-1) * (m_size-1));}
    GLsizei sizeOfIndices() const {return numIndices() * sizeof(GLuint);}
    const void* indices() const {return m_indices;}
};

// ====================================== WaterSurface class ==========================================//
class WaterSurface : public Geom
{
private:
    GLfloat m_height;
    GLfloat m_edgeLength;
    GLfloat m_LoDFactor;
    int m_numQuadsPerEdge;
    int m_hmapSize; // heightmap is m_hmapSize x m_hmapSize

    GLuint *m_indices;
    vec2f *m_positions;
    vec2f *m_texCoords;

    void genMesh();
    void genPositions();
    void genIndices();
    void genTexCoords();
public:
    WaterSurface(int _numQuadsPerEdge, int _hmapSize);
    ~WaterSurface();
    GLfloat height() const {return m_height;}
    GLfloat edgeLength() const {return m_edgeLength;}
    GLfloat LoDFactor() const {return m_LoDFactor;}
    void setNumQuadsPerEdge(int _numQuadsPerEdge) {m_numQuadsPerEdge = _numQuadsPerEdge; genMesh();}
    int numQuadsPerEdge() const {return m_numQuadsPerEdge;}
    void setHmapSize(int _hmapSize) {m_hmapSize = _hmapSize;}
    int hmapSize() const {return m_hmapSize;}
    GLint numTexelsPerQuadEdge() const {return static_cast<GLint>(m_hmapSize / m_numQuadsPerEdge);}
    GLint maxTessLevel() const;
    vec2i texelIPos(const vec3f &worldPos) const;
    vec2i texelIPos(const QVector3D &worldPos) const;
    GLfloat texelDist(GLfloat worldDist) const;
    GLfloat world2TexelFactor() const;
    int idxAt(int z, int x) const;

    GLsizei numVertices() const {return static_cast<GLsizei>(std::pow(m_numQuadsPerEdge + 1, 2));}
    GLint lenOfPosition() const {return 2;}
    GLsizei sizeOfPositions() const {return numVertices() * sizeof(vec2f);}
    const void* positions() const {return m_positions;}

    const void* texCoords() const {return m_texCoords;}
    GLint lenOfTexCoord() const {return 2;}
    GLsizei sizeOfTexCoords() const {return static_cast<GLsizei>(numVertices() * sizeof(vec2f));}

    GLsizei numIndices() const {return static_cast<GLsizei>(4 * m_numQuadsPerEdge * m_numQuadsPerEdge);}
    GLsizei sizeOfIndices() const {return numIndices() * sizeof(GLuint);}
    const void* indices() const {return m_indices;}
};

// ====================================== ScreenQuad class ==========================================//
class ScreenQuad {
private:
    vec2f m_positions[4];
    vec2f m_texCoords[4];
public:
    ScreenQuad();
    ~ScreenQuad() {}
    GLint lenOfPosition() const {return 2;}
    GLint lenOfTexCoord() const {return 2;}
    GLsizei sizeOfPositions() const {return numVertices() * sizeof(vec2f);}
    GLsizei sizeOfTexCoords() const {return numVertices() * sizeof(vec2f);}
    GLsizei numVertices () const {return 4;}
    const void* positions() const {return static_cast<const void*>(m_positions);}
    const void* texCoords() const {return static_cast<const void*>(m_texCoords);}
};

#endif // GEOMS_H
