#include "glhelper.h"

// ========================================== Functions ===============================================//
std::string
glErrorToString(GLenum err)
{
    switch (err) {
    case GL_INVALID_OPERATION:
        return std::string("GL_INVALID_OPERATION");
    case GL_INVALID_ENUM:
        return std::string("GL_INVALID_ENUM");
    case GL_INVALID_VALUE:
        return std::string("GL_INVALID_VALUE");
    case GL_OUT_OF_MEMORY:
        return std::string("GL_OUT_OF_MEMORY");
// return by glCheckFramebufferStatus()
    case GL_FRAMEBUFFER_COMPLETE:
        return std::string("GL_FRAMEBUFFER_COMPLETE");
    case GL_INVALID_FRAMEBUFFER_OPERATION:
        return std::string("GL_INVALID_FRAMEBUFFER_OPERATION");
    case GL_FRAMEBUFFER_UNDEFINED:
        return std::string("GL_FRAMEBUFFER_UNDEFINED");
    case GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT:
        return std::string("GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT");
    case GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT:
        return std::string("GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT");
    case GL_FRAMEBUFFER_INCOMPLETE_DRAW_BUFFER:
        return std::string("GL_FRAMEBUFFER_INCOMPLETE_DRAW_BUFFER");
    case GL_FRAMEBUFFER_INCOMPLETE_READ_BUFFER:
        return std::string("GL_FRAMEBUFFER_INCOMPLETE_READ_BUFFER");
    case GL_FRAMEBUFFER_UNSUPPORTED:
        return std::string("GL_FRAMEBUFFER_UNSUPPORTED");
    case GL_FRAMEBUFFER_INCOMPLETE_MULTISAMPLE:
        return std::string("GL_FRAMEBUFFER_INCOMPLETE_MULTISAMPLE");
    default:
        return std::string("UNKOWN ERROR: " ) + std::to_string(err);
    }
}


// ===================================== RandVecTex class =============================================//
RandVecTex::RandVecTex(GLsizei _size, GLfloat _n)
{
    m_size = _size;
    m_n = _n;
    m_tex = nullptr;
    refresh();
}

void
RandVecTex::refresh()
{
    if (m_tex != nullptr) delete [] m_tex;
    m_tex = new vec3f[numTexels()];
    randDirs(numTexels(), m_tex, m_n);
}

void
RandVecTex::setN(GLsizei _n)
{
    m_n = _n;
    refresh();
}

void
RandVecTex::setSize(GLsizei _size)
{
    m_size = _size;
    refresh();
}

// ======================================= Texture class ==============================================//
Texture::Texture(const QString &_filePath)
{
    m_filePath = _filePath;
    QImage img{m_filePath};
    if (img.isNull()) {
        throw Error(QString("Open Image File %1 failed.").arg(m_filePath).toStdString());
    }
    m_width = img.width();
    m_height = img.height();
    m_tex = new GLfloat[numTexels() * 3];
    for (int y = 0; y < m_height; ++y) {
        for (int x = 0; x < m_width; ++x) {
            QRgb rgb = img.pixel(x, y);
            QColor color{rgb};
            setTexel(x, y, color);
        }
    }
}

void
Texture::setTexel(int x, int y, QColor c)
{
    int i = idx(x, y);
    m_tex[i]   = static_cast<GLfloat>(c.redF());
    m_tex[i+1] = static_cast<GLfloat>(c.greenF());
    m_tex[i+2] = static_cast<GLfloat>(c.blueF());
}

GLsizei
Texture::mipmapLevels() const
{
    return static_cast<GLsizei>(std::log2(std::min(m_width, m_height)) + 1);
}

// ==================================== ShaderProgram class ===========================================//
ShaderProgram::ShaderProgram()
{
    initializeOpenGLFunctions();
    m_shaderProg = glCreateProgram();
}

ShaderProgram::~ShaderProgram()
{
    glDeleteProgram(m_shaderProg);
}

void
ShaderProgram::attachShader(Shader *shader)
{
    glAttachShader(m_shaderProg, shader->shaderObj());
    m_shaders.push_back(shader->shaderObj());
}

void
ShaderProgram :: detachShader(Shader* shader) {
    glDetachShader(m_shaderProg, shader->shaderObj());
    for (size_t i = 0; i < m_shaders.size(); ++i) {
        if (m_shaders[i] == shader->shaderObj()) {
            m_shaders.erase(m_shaders.begin() + i);
            break;
        }
    }
}

void
ShaderProgram::detachAll()
{
    for (size_t i = 0; i < m_shaders.size(); ++i) {
        glDetachShader(m_shaderProg, m_shaders[i]);
    }
    m_shaders.clear();
}

bool
ShaderProgram::link()
{
    glLinkProgram(m_shaderProg);
    GLint linkProgResult = GL_TRUE;
    glGetProgramiv(m_shaderProg, GL_LINK_STATUS, &linkProgResult);
    if (linkProgResult == GL_FALSE) {
        GLint logLength = 0;

        glGetProgramiv(m_shaderProg, GL_INFO_LOG_LENGTH, &logLength);
        GLchar* infoLogBuffer = new GLchar[logLength];

        glGetProgramInfoLog(m_shaderProg, logLength, NULL, infoLogBuffer);
        std::string errorMsg(infoLogBuffer);

        delete[] infoLogBuffer;
        std::cout << errorMsg.c_str() << std::endl;
        return false;
    }
    return true;
}

GLuint
ShaderProgram::operator[] (const GLchar *uniform)
{
    GLuint result = -1;
    try {
        result = m_uniformLocs.at(std::string{uniform});
    } catch (std::out_of_range) {
        std::cout << "uniform " << uniform << "does not exist." << std::endl;
    }
    return result;
}

void
ShaderProgram::addUniform(const GLchar* uniform)
{
    m_uniformLocs[std::string{uniform}] = getUniformLocation(uniform);
}

void
ShaderProgram::addUniforms(int numUniforms, const GLchar *uniforms[])
{
    for (int i = 0; i < numUniforms; ++i) {
        addUniform(uniforms[i]);
    }
}

// =========================================== Shader class ===========================================//
Shader::Shader(const char *fileName, GLenum shaderType)
{
    initializeOpenGLFunctions();
    m_shaderObj = glCreateShader(shaderType);
    std::vector<std::string> srcStrings = readFile(fileName);
    size_t srcNumLines = srcStrings.size();
    GLchar** srcCstrs = new GLchar*[srcNumLines];

    for (size_t i = 0; i < srcNumLines; i++) {
      srcCstrs[i] = new GLchar[srcStrings[i].length() + 1];
      strcpy(srcCstrs[i], srcStrings[i].c_str());
    }

    glShaderSource(m_shaderObj, srcNumLines, (const char**)srcCstrs, NULL);

    for (size_t j = 0; j < srcNumLines; ++j) {
        delete[] srcCstrs[j];
    }
    delete[] srcCstrs;

    compile();
}

std::vector<std::string>
Shader::readFile(const char *fileName)
{
    std::vector<std::string> srcStrings;
    std::ifstream shaderSrc;
      shaderSrc.open(fileName);
      if (!shaderSrc.is_open()) {
          std::cout << fileName << " does not exist" << std::endl;
          throw Error{QString("%1 does not exist").arg(fileName)};
      }
      bool readStarted = false;
      while (!shaderSrc.eof()) {
          std::string temp;
          std::getline(shaderSrc, temp);
          if (temp.find("#version") != std::string::npos) {
              readStarted = true;
          }
          if (!readStarted) continue;
          temp = temp + "\n";
          srcStrings.push_back(temp);
        }
        shaderSrc.close();
        return srcStrings;
}

void
Shader::compile() {
    glCompileShader(m_shaderObj);

    GLint gl_CompileResult = 1;
    glGetShaderiv(m_shaderObj, GL_COMPILE_STATUS, &gl_CompileResult);
    if (gl_CompileResult == GL_FALSE) {
        GLint gl_LogLength;
        glGetShaderiv(m_shaderObj, GL_INFO_LOG_LENGTH, &gl_LogLength);
        GLchar* gl_InfoLogBuffer = new GLchar[gl_LogLength];
        glGetShaderInfoLog(m_shaderObj, gl_LogLength, NULL, gl_InfoLogBuffer);
        std::string error_msg = std::string(gl_InfoLogBuffer);
        delete[] gl_InfoLogBuffer;
        std::cout << error_msg.c_str() << std::endl;
    }
}

