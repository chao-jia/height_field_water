#include "glwidget.h"

void
GLWidget::catchGLError(const QString& segmntName)
{
    GLenum err;
    bool good = true;

    while(( err = glGetError()) != GL_NO_ERROR) {
        good = false;
        std::cout << "After " << segmntName << ": " << std::flush;
        std::cout << glErrorToString(err) << " " << std::endl;
    }
    if (good) {
        // std::cout << "After " << segmntName << ": No GL Error" << std::endl;
    }
}

GLWidget::GLWidget(QWidget *parent) : QOpenGLWidget(parent)
{
    setFocusPolicy(Qt::WheelFocus);
    INTERVAL = 10;
    m_wireFrameOn = false;
    m_inGravityMode = false;
    m_paused = false;

    m_writeTexIdx = 0;
    m_hmapSize = 128;
    m_wsNumQuadsPerEdge = 32;
    m_numSampledRays = 2; //
    m_recursionDepth = 2; //
    m_shadowMapSize = 256;

    m_rippleWorldPos = QVector3D(FLT_MAX, FLT_MAX, FLT_MAX);

    m_hmap = nullptr;
    m_waterSurface = nullptr;

    m_hmapDiffuseProg = nullptr;
    m_hmapNormalProg = nullptr;
    m_hmapPosRenderProg = nullptr;
    m_hmapNormalRenderProg = nullptr;
    m_screenQuadProg = nullptr;
    m_poolProg = nullptr;
    m_sphereProg = nullptr;
    m_waterSurfaceProg = nullptr;

    m_cam = new Cam();
    m_persp = new PerspectiveProj(width(), height());

    m_DistlightPos2Plane = 1.f;
    m_lightPosPlane = 4.5f;
    setupLightDir();
    GLfloat lMax = 0.65f;
    m_ILMax = QVector3D(lMax, lMax, lMax);
    m_sExp = 1.f;
    m_penumbraAngle = static_cast<float>(M_PI) / 16.f;
    m_umbraAngle = static_cast<float>(M_PI) / 3.f;

    m_pickingID = pickingID(GeomType::UNKNOWN);
    m_contentIdx = SCENE;

    m_fpsCounter = 0;
    m_sumPaintTime = 0;
    MAX_FPS_COUNT = 10;

    m_timer = new QTimer();
    connect(m_timer, SIGNAL(timeout()), this, SLOT(update()));
    m_timer->start(INTERVAL);
}

void
GLWidget::initializeGL()
{
    initializeOpenGLFunctions();
    glGenFramebuffers(1, &m_pickingFBO);
    glGenFramebuffers(1, &m_hmapDiffuseFBO);
    glGenFramebuffers(1, &m_shadowMapFBO);
    setupPickingFBO();
    setupShadowMapFBO();
    glGenBuffers(1, &m_pickingPpBO);
    glGenBuffers(1, &m_worldPosPpBo);
    glGenBuffers(1, &m_normalPpBo);

    setupScreenQuad();
    setupPool();
    setupWaterSurface();
    setupHeightmap();
    setupSphere();
    genShaderProg();
    catchGLError("initializeGL->end");
}

void
GLWidget::resizeGL(int w, int h)
{
    m_persp->setAspectRatio(w, h);
    setupPickingFBO();
}

// update sphere position
// wave from sphere movement
// calc wave (ripples, sphere movement),
// diffuse heightmap, calc heightmap normal, then call update
void
GLWidget::updateScene()
{
    if (!m_paused) {
        if (m_inGravityMode) {
            m_sphere->moveInGravityMode(INTERVAL / 1000.f);
        }
        diffuseHmap();
        catchGLError("updateScene->diffuseHmap");
        calcHmapNormal();
        catchGLError("updateScene->calcHmapNormal");
        calcCaustics();
        catchGLError("updateScene->calcCaustics");
        m_sphereLastPos = m_sphere->translation();
    }
    // m_paused = true;
}

void
GLWidget::paintGL()
{
    if (m_fpsCounter == 0) m_fpsTimer.start();
    updateScene();
    catchGLError("paintGL->updateScene");
    glCullFace(GL_FRONT);
    genShadowMap();
    catchGLError("paintGL->genShadowMap");
    glCullFace(GL_BACK);
    drawScene();
    catchGLError("paintGL->drawScene");
    drawScreenQuad();
    catchGLError("paintGL->drawScreenQuad");
    m_fpsCounter++;
    if (m_fpsCounter == MAX_FPS_COUNT) {
        m_sumPaintTime = m_fpsTimer.elapsed();
        float fps = 1000.f *
                    static_cast<float>(MAX_FPS_COUNT) /
                    static_cast<float>(m_sumPaintTime);
        m_sumPaintTime = 0;
        m_fpsCounter = 0;
        emit FPSChanged(fps);
    }

}

void
GLWidget::keyPressEvent(QKeyEvent *event)
{
    switch (event->modifiers()) {
    case Qt::ControlModifier:
        switch (event->key()) {
        case Qt::Key_S:
            // setting dialog
            break;
        }
        break;
    case Qt::ShiftModifier:
        switch (event->key()) {
        case Qt::Key_W:
            m_wireFrameOn = !m_wireFrameOn;
            break;
        case Qt::Key_R:
            m_cam->reset();
            break;
        default:
            break;
        }
        break;
    case Qt::KeypadModifier:
    case Qt::NoModifier:
        switch (event->key()) {
        case Qt::Key_Escape:
            m_contentIdx = SCENE;
            break;
        case Qt::Key_S:
            m_contentIdx = SHADOW_MAP_TEX;
            break;
        case Qt::Key_N:
            m_contentIdx = HM_NOR_TEX;
            break;
        case Qt::Key_P:
            m_contentIdx = HM_POS_TEX;
            break;
        case Qt::Key_V:
            m_contentIdx = HM_VEL_TEX;
            break;
        case Qt::Key_C:
            m_contentIdx = CAUSTICS_TEX;
            break;
        case Qt::Key_M:
            m_contentIdx = HM_MESH_TEST;
            break;
        case Qt::Key_F5:
            refreshShaderProg();
            break;
        case Qt::Key_G:
            m_inGravityMode = !m_inGravityMode;
            m_sphere->setVelocity(0.f);
            break;
        case Qt::Key_L:
            setupLightDir();
            break;
        case Qt::Key_Space:
            m_paused = !m_paused;
            break;
        }
        break;
    }
}

void
GLWidget::mousePressEvent(QMouseEvent *event)
{
    m_mouseLastPos = toGLWndCoord(event->pos());
    m_pickingID = pick(m_mouseLastPos);
    std::cout << "pick id = " << static_cast<unsigned>(m_pickingID) << std::endl;
    locationNormal(m_mouseLastPos);
    locateRipple(m_mouseLastPos);
}

void
GLWidget::mouseReleaseEvent(QMouseEvent *)
{
    m_pickingID = pickingID(GeomType::UNKNOWN);
}

void
GLWidget::mouseMoveEvent(QMouseEvent *event)
{
    QPoint currentPos = toGLWndCoord(event->pos());
    QPointF lastGLPos = toNDC(m_mouseLastPos);
    QPointF currentGLPos = toNDC(currentPos);
    if (m_pickingID == m_sphere->pickColor()) {
        QPointF delta = currentGLPos - lastGLPos;
        QVector3D deltaView(delta.x(), delta.y(), 0.f);
        QVector3D deltaWorld = 2.5 * QVector3D(m_cam->invertedViewMat() * QVector4D(deltaView, 0.f));
        m_sphere->moveInUserManipMode(deltaWorld);
    } else if (m_pickingID == m_waterSurface->pickColor()) {
        locateRipple(currentPos);
    } else {
        QPointF rotationCenter = toNDC(m_mouseLastPos);
        m_cam->orbit(rotationCenter, lastGLPos, currentGLPos);
    }
    m_mouseLastPos = currentPos;
}

QPoint
GLWidget::toGLWndCoord(const QPoint &p) const
{
    return QPoint(p.x(), height() - p.y());
}

QPointF
GLWidget::toNDC(const QPoint &p) const
{
    float x = (static_cast<float>(p.x()) / width()) * 2.f -1.f;
    float y = (static_cast<float>(p.y()) / height() ) * 2.f - 1.f;
    return QPointF(x, y);
}

void
GLWidget::setupLightDir()
{
    m_lightDir = m_cam->lookAt();
    m_lightDir.setY(-std::abs(m_lightDir.y()));
    m_lightDir.normalize();
    m_lightDir.setY(clamp(m_lightDir.y(), -1.f, -0.75f));
    m_lightDir.normalize();
    m_lightPos = (m_lightPosPlane / m_lightDir.y()) * m_lightDir;
}

void
GLWidget::setupPickingFBO()
{
    if (glIsTexture(m_pickingTexes[0])) {
        glDeleteTextures(4, m_pickingTexes);
    }
    glGenTextures(5, m_pickingTexes);
    glBindFramebuffer(GL_FRAMEBUFFER, m_pickingFBO);

    glBindTexture(GL_TEXTURE_2D, m_pickingTexes[0]);
    glTexStorage2D(GL_TEXTURE_2D, 1, GL_RGBA8, width(), height());
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    glBindTexture(GL_TEXTURE_2D, m_pickingTexes[1]);
    glTexStorage2D(GL_TEXTURE_2D, 1, GL_R8, width(), height());
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    glBindTexture(GL_TEXTURE_2D, m_pickingTexes[2]);
    glTexStorage2D(GL_TEXTURE_2D, 1, GL_RGB32F, width(), height());
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    glBindTexture(GL_TEXTURE_2D, m_pickingTexes[3]);
    glTexStorage2D(GL_TEXTURE_2D, 1, GL_RGB32F, width(), height());
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    glBindTexture(GL_TEXTURE_2D, m_pickingTexes[4]);
    glTexStorage2D(GL_TEXTURE_2D, 1, GL_DEPTH_COMPONENT32F, width(), height());

    glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, m_pickingTexes[0], 0);
    glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, m_pickingTexes[1], 0);
    glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT2, m_pickingTexes[2], 0);
    glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT3, m_pickingTexes[3], 0);
    glFramebufferTexture(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, m_pickingTexes[4], 0);
}

void
GLWidget::setupShadowMapFBO()
{
    if (glIsTexture(m_shadowMapTex)) {
        glDeleteTextures(1, &m_shadowMapTex);
    }

    glBindFramebuffer(GL_FRAMEBUFFER, m_shadowMapFBO);
    glGenTextures(1, &m_shadowMapTex);
    glBindTexture(GL_TEXTURE_2D,m_shadowMapTex);
    glTexStorage2D(GL_TEXTURE_2D, 1, GL_DEPTH_COMPONENT32F, m_shadowMapSize, m_shadowMapSize);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glFramebufferTexture(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, m_shadowMapTex, 0);
    glDrawBuffer(GL_NONE);
    if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE) {
        std::cout << "shadow buffer fbo not complete" << std::endl;
    } else {
        std::cout << "shadow buffer fbo complete" << std::endl;
    }
}

void
GLWidget::setupScreenQuad()
{
    glGenVertexArrays(1, &m_screenQuadVao);
    glBindVertexArray(m_screenQuadVao);
    glGenBuffers(2, m_screenQuadBos);
    ScreenQuad sq;

    glBindBuffer(GL_ARRAY_BUFFER, m_screenQuadBos[0]);
    glBufferData(GL_ARRAY_BUFFER, sq.sizeOfPositions(), sq.positions(), GL_STATIC_DRAW);
    glVertexAttribPointer(0, sq.lenOfPosition(), GL_FLOAT, GL_FALSE, 0, 0);
    glEnableVertexAttribArray(0);

    glBindBuffer(GL_ARRAY_BUFFER, m_screenQuadBos[1]);
    glBufferData(GL_ARRAY_BUFFER, sq.sizeOfTexCoords(), sq.texCoords(), GL_STATIC_DRAW);
    glVertexAttribPointer(1, sq.lenOfTexCoord(), GL_FLOAT, GL_FALSE, 0, 0);
    glEnableVertexAttribArray(1);
}

void
GLWidget::setupPool()
{
    glGenVertexArrays(1, &m_poolVao);
    glBindVertexArray(m_poolVao);
    glGenBuffers(3, m_poolBos);
    m_pool = new Pool();
    glBindBuffer(GL_ARRAY_BUFFER, m_poolBos[0]);
    glBufferData(GL_ARRAY_BUFFER, m_pool->sizeOfPositions(), m_pool->positions(), GL_STATIC_DRAW);
    glVertexAttribPointer(0, m_pool->lenOfPosition(), GL_FLOAT, GL_FALSE, 0, 0);
    glEnableVertexAttribArray(0);

    glBindBuffer(GL_ARRAY_BUFFER, m_poolBos[1]);
    glBufferData(GL_ARRAY_BUFFER, m_pool->sizeOfNormals(), m_pool->normals(), GL_STATIC_DRAW);
    glVertexAttribPointer(1, m_pool->lenOfNormal(), GL_FLOAT, GL_FALSE, 0, 0);
    glEnableVertexAttribArray(1);

    glBindBuffer(GL_ARRAY_BUFFER, m_poolBos[2]);
    glBufferData(GL_ARRAY_BUFFER, m_pool->sizeOfTexCoords(), m_pool->texCoords(), GL_STATIC_DRAW);
    glVertexAttribPointer(2, m_pool->lenOfTexCoord(), GL_FLOAT, GL_FALSE, 0, 0);
    glEnableVertexAttribArray(2);

    catchGLError("setupPool->glEnableVertexAttribArray2");
    glGenTextures(1, &m_tileTex);
    Texture tile("../img/tiles.jpg");
    m_tileTexSize = tile.width();
    glPixelStorei(GL_UNPACK_ALIGNMENT, 4);
    glBindTexture(GL_TEXTURE_2D, m_tileTex);
    glTexStorage2D(GL_TEXTURE_2D, tile.mipmapLevels(),
                   GL_RGB32F, tile.width(), tile.height());
    glTexSubImage2D(GL_TEXTURE_2D, 0,
                    0, 0, tile.width(), tile.height(),
                    GL_RGB, GL_FLOAT,
                    tile.tex());
    glGenerateMipmap(GL_TEXTURE_2D);
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR_MIPMAP_LINEAR);
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    catchGLError("setupPool->end");
}

void
GLWidget::setupSphere()
{
    glGenVertexArrays(1, &m_sphereVao);
    glBindVertexArray(m_sphereVao);
    glGenBuffers(2, m_sphereBos);
    m_sphere = new Sphere(m_waterSurface->height(), m_pool->edgeLength(), m_pool->height());
    m_sphereLastPos = m_sphere->translation();

    glBindBuffer(GL_ARRAY_BUFFER, m_sphereBos[0]);
    glBufferData(GL_ARRAY_BUFFER, m_sphere->sizeOfPositions(), m_sphere->positions(), GL_STATIC_DRAW);
    glVertexAttribPointer(0, m_sphere->lenOfPosition(), GL_FLOAT, GL_FALSE, 0, 0);
    glEnableVertexAttribArray(0);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_sphereBos[1]);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, m_sphere->sizeOfIndices(), m_sphere->indices(), GL_STATIC_DRAW);
}

void
GLWidget::setupHeightmap()
{
    if (!glIsVertexArray(m_hmapVao)) {
        glGenVertexArrays(1, &m_hmapVao);
    }
    if (!glIsBuffer(m_hmapBos[0])) {
        glGenBuffers(2, m_hmapBos);
    }
    glBindVertexArray(m_hmapVao);
    if (m_hmap != nullptr) {
        delete m_hmap;
    }
    m_hmap = new Heightmap(m_hmapSize);

    glBindBuffer(GL_ARRAY_BUFFER, m_hmapBos[0]);
    glBufferData(GL_ARRAY_BUFFER, m_hmap->sizeOfIPositions(), m_hmap->iPositions(), GL_STATIC_DRAW);
    glVertexAttribIPointer(0, m_hmap->lenOfIPosition(), GL_INT, 0, 0);
    glEnableVertexAttribArray(0);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_hmapBos[1]);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER,m_hmap->sizeOfIndices(), m_hmap->indices(), GL_STATIC_DRAW);

    m_initTexPos = new GLfloat[m_hmap->numVertices()];
    m_initTexVel = new GLfloat[m_hmap->numVertices()];
    // int upper = m_hmap->size() * 0.53;
    // int lower = m_hmap->size() * 0.47;
    int m = m_hmap->size() * 0.7;
    for (int j = 0; j < m_hmap->size(); ++j) {
        for (int i = 0; i < m_hmap->size(); ++i) {
            int idx = j * m_hmap->size() + i;
            m_initTexPos[idx] = 0.f;
            m_initTexVel[idx] = 0.f;
            if(std::sqrt((i - m) * (i - m) + (j - m) * (j - m)) < 16) {
                m_initTexVel[idx] = 0.f;
            }
        }
    }

    if (glIsTexture(m_hmapPosTex[0])) {
        glDeleteTextures(2, m_hmapPosTex);
        glDeleteTextures(2, m_hmapVelocityTex);
        glDeleteTextures(1, &m_hmapNormalTex);
        glDeleteTextures(1, &m_hmapDepthTex);
        glDeleteTextures(1, &m_causticsTex);
    }
    glBindFramebuffer(GL_FRAMEBUFFER, m_hmapDiffuseFBO);
    glGenTextures(2, m_hmapPosTex);
    glGenTextures(2, m_hmapVelocityTex);
    glGenTextures(1, &m_hmapNormalTex);
    glGenTextures(1, &m_causticsTex);
    glGenTextures(1, &m_hmapDepthTex);
    glBindTexture(GL_TEXTURE_2D, m_hmapPosTex[0]);
    glTexStorage2D(GL_TEXTURE_2D, 1, GL_R32F, m_hmap->size(), m_hmap->size());
    glTexSubImage2D(GL_TEXTURE_2D, 0,
                    0, 0, m_hmap->size(), m_hmap->size(),
                    GL_RED, GL_FLOAT,
                    m_initTexPos);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    glBindTexture(GL_TEXTURE_2D, m_hmapPosTex[1]);
    glTexStorage2D(GL_TEXTURE_2D, 1, GL_R32F, m_hmap->size(), m_hmap->size());
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    glBindTexture(GL_TEXTURE_2D, m_hmapVelocityTex[0]);
    glTexStorage2D(GL_TEXTURE_2D, 1, GL_R32F, m_hmap->size(), m_hmap->size());
    glTexSubImage2D(GL_TEXTURE_2D, 0,
                    0, 0, m_hmap->size(), m_hmap->size(),
                    GL_RED, GL_FLOAT,
                    m_initTexVel);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    glBindTexture(GL_TEXTURE_2D, m_hmapVelocityTex[1]);
    glTexStorage2D(GL_TEXTURE_2D, 1, GL_R32F, m_hmap->size(), m_hmap->size());
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    glBindTexture(GL_TEXTURE_2D, m_hmapNormalTex);
    glTexStorage2D(GL_TEXTURE_2D, 1, GL_RGB32F, m_hmap->size(), m_hmap->size());
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    glBindTexture(GL_TEXTURE_2D, m_causticsTex);
    glTexStorage2D(GL_TEXTURE_2D, 1, GL_R32F, m_hmap->size(), m_hmap->size());
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    glBindTexture(GL_TEXTURE_2D, m_hmapDepthTex);
    glTexStorage2D(GL_TEXTURE_2D, 1, GL_DEPTH_COMPONENT32F, width(), height());

    glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, m_hmapPosTex[0], 0);
    glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, m_hmapPosTex[1], 0);
    glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT2, m_hmapVelocityTex[0], 0);
    glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT3, m_hmapVelocityTex[1], 0);
    glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT4, m_hmapNormalTex, 0);
    glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT5, m_causticsTex, 0);
    glFramebufferTexture(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, m_hmapDepthTex, 0);
}

void
GLWidget::setupWaterSurface()
{
    if (!glIsVertexArray(m_waterSurfaceVao)) {
        glGenVertexArrays(1, &m_waterSurfaceVao);
    }
    if (!glIsBuffer(m_waterSurfaceBos[0])) {
        glGenBuffers(3, m_waterSurfaceBos);
    }
    glBindVertexArray(m_waterSurfaceVao);

    if (m_waterSurface != nullptr) {
        delete m_waterSurface;
    }
    m_waterSurface = new WaterSurface(m_wsNumQuadsPerEdge, m_hmapSize);

    glBindBuffer(GL_ARRAY_BUFFER, m_waterSurfaceBos[0]);
    glBufferData(GL_ARRAY_BUFFER, m_waterSurface->sizeOfPositions(),
                 m_waterSurface->positions(), GL_STATIC_DRAW);
    glVertexAttribPointer(0, m_waterSurface->lenOfPosition(), GL_FLOAT, GL_FALSE, 0, 0);
    glEnableVertexAttribArray(0);

    glBindBuffer(GL_ARRAY_BUFFER, m_waterSurfaceBos[1]);
    glBufferData(GL_ARRAY_BUFFER, m_waterSurface->sizeOfTexCoords(),
                 m_waterSurface->texCoords(), GL_STATIC_DRAW);
    glVertexAttribPointer(1, m_waterSurface->lenOfTexCoord(), GL_FLOAT, GL_FALSE, 0, 0);
    glEnableVertexAttribArray(1);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_waterSurfaceBos[2]);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, m_waterSurface->sizeOfIndices(),
                 m_waterSurface->indices(), GL_STATIC_DRAW);

}


void
GLWidget::genShaderProg()
{
    Shader hmapDiffuseVert("../shaders/hmap_diffuse.vert", GL_VERTEX_SHADER);
    Shader hmapDiffuseFrag("../shaders/hmap_diffuse.frag", GL_FRAGMENT_SHADER);
    m_hmapDiffuseProg = new ShaderProgram();
    m_hmapDiffuseProg->attachShader(&hmapDiffuseVert);
    m_hmapDiffuseProg->attachShader(&hmapDiffuseFrag);
    m_hmapDiffuseProg->link();
    const GLchar *hmapDiffuseUniforms[] = {"deltaT", "cOVERh_squared", "hmapSize",
                                           "fMax", "velocityDampFactor", "rippleILoc",
                                           "worldEdgeLen", "spherePos", "lastSpherePos",
                                           "waterSurfaceH","spehreRadius", "coSphereWave",
                                           "coRipple"};
    m_hmapDiffuseProg->addUniforms(13, hmapDiffuseUniforms);

    Shader hmapNormalVert("../shaders/hmap_normal.vert", GL_VERTEX_SHADER);
    Shader hmapNormalFrag("../shaders/hmap_normal.frag", GL_FRAGMENT_SHADER);
    m_hmapNormalProg = new ShaderProgram();
    m_hmapNormalProg->attachShader(&hmapNormalVert);
    m_hmapNormalProg->attachShader(&hmapNormalFrag);
    m_hmapNormalProg->link();
    const GLchar *hmapNormalUniforms[] = {"worldEdgeLen", "hmapSize"};
    m_hmapNormalProg->addUniforms(2, hmapNormalUniforms);

    Shader causticsCalcVert("../shaders/caustics_calc.vert", GL_VERTEX_SHADER);
    Shader causticsCalcFrag("../shaders/caustics_calc.frag", GL_FRAGMENT_SHADER);
    m_causticsCalcProg = new ShaderProgram();
    m_causticsCalcProg->attachShader(&causticsCalcVert);
    m_causticsCalcProg->attachShader(&causticsCalcFrag);
    m_causticsCalcProg->link();
    const GLchar *causticsUniforms[] = {"hmapSize", "coCausticsDF",
                                        "deltaCausticsDF", "maxCausticsRatio"};
    m_causticsCalcProg->addUniforms(4, causticsUniforms);

    Shader hmapTexPosVert("../shaders/screen_quad.vert", GL_VERTEX_SHADER);
    Shader hmapTexPosFrag("../shaders/hmap_tex_pos.frag", GL_FRAGMENT_SHADER);
    m_hmapPosRenderProg = new ShaderProgram();
    m_hmapPosRenderProg->attachShader(&hmapTexPosVert);
    m_hmapPosRenderProg->attachShader(&hmapTexPosFrag);
    m_hmapPosRenderProg->link();
    const GLchar *hmapPosRenderUniforms[] = {"posFactor"};
    m_hmapPosRenderProg->addUniforms(1, hmapPosRenderUniforms);

    Shader hmapTexNormVert("../shaders/screen_quad.vert", GL_VERTEX_SHADER);
    Shader hmapTexNormFrag("../shaders/hmap_tex_norm.frag", GL_FRAGMENT_SHADER);
    m_hmapNormalRenderProg = new ShaderProgram();
    m_hmapNormalRenderProg->attachShader(&hmapTexNormVert);
    m_hmapNormalRenderProg->attachShader(&hmapTexNormFrag);
    m_hmapNormalRenderProg->link();

    const GLchar *screenUniforms[] = {"camPos", "lightPos", "lightDir", "lightPlaneH",
                                      "lightIMax", "lightExp", "cosTp", "cosTu",
                                      "sphereCenter", "sphereRadius", "sphereKd", "sphereID",
                                      "poolEdgeLen", "poolID", "poolHeight", "tileTexWorldLen",
                                      "tileTexSize", "waterSurfaceH", "waterSurfaceID", "hmapSize",
                                      "shadowMapSize", "shadowBiasVP"};
    int numscreenUniforms = 22;
    Shader screenQuadVert("../shaders/screen_quad.vert", GL_VERTEX_SHADER);
    Shader screenQuadFrag("../shaders/screen_quad.frag", GL_FRAGMENT_SHADER);
    m_screenQuadProg = new ShaderProgram();
    m_screenQuadProg->attachShader(&screenQuadVert);
    m_screenQuadProg->attachShader(&screenQuadFrag);
    m_screenQuadProg->link();
    m_screenQuadProg->addUniforms(numscreenUniforms, screenUniforms);

    Shader poolVert("../shaders/pool.vert", GL_VERTEX_SHADER);
    Shader poolFrag("../shaders/pool.frag", GL_FRAGMENT_SHADER);
    m_poolProg = new ShaderProgram();
    m_poolProg->attachShader(&poolVert);
    m_poolProg->attachShader(&poolFrag);
    m_poolProg->link();
    const GLchar *poolUniforms[] = {"VP", "pickingID"};
    int numPoolUniforms = 2;
    m_poolProg->addUniforms(numPoolUniforms , poolUniforms);

    Shader sphereVert("../shaders/sphere.vert", GL_VERTEX_SHADER);
    Shader sphereTes("../shaders/sphere.tes", GL_TESS_EVALUATION_SHADER);
    Shader sphereFrag("../shaders/sphere.frag", GL_FRAGMENT_SHADER);
    m_sphereProg = new ShaderProgram();
    m_sphereProg->attachShader(&sphereVert);
    m_sphereProg->attachShader(&sphereTes);
    m_sphereProg->attachShader(&sphereFrag);
    m_sphereProg->link();
    const GLchar *sphereUniforms[] = {"VP", "sphereRadius", "sphereCenter", "pickingID"};
    int numSphereUniforms = 4;
    m_sphereProg->addUniforms(numSphereUniforms , sphereUniforms);

    Shader waterSurfaceVert("../shaders/water_surface.vert", GL_VERTEX_SHADER);
    Shader waterSurfaceTcs("../shaders/water_surface.tcs", GL_TESS_CONTROL_SHADER);
    Shader waterSurfaceTes("../shaders/water_surface.tes", GL_TESS_EVALUATION_SHADER);
    Shader waterSurfaceFrag("../shaders/water_surface.frag", GL_FRAGMENT_SHADER);
    m_waterSurfaceProg = new ShaderProgram();
    m_waterSurfaceProg->attachShader(&waterSurfaceVert);
    m_waterSurfaceProg->attachShader(&waterSurfaceTcs);
    m_waterSurfaceProg->attachShader(&waterSurfaceTes);
    m_waterSurfaceProg->attachShader(&waterSurfaceFrag);
    m_waterSurfaceProg->link();
    const GLchar *waterSurfaceUniforms[] = {"VP", "waterSurfaceH", "pickingID"};
    int numWaterSurfaceUniforms = 3;
    m_waterSurfaceProg->addUniforms(numWaterSurfaceUniforms , waterSurfaceUniforms);

    Shader shadowMapTexVert("../shaders/screen_quad.vert", GL_VERTEX_SHADER);
    Shader shadowMapTexFrag("../shaders/shadowmap_tex.frag", GL_FRAGMENT_SHADER);
    m_drawShadowMapTexProg = new ShaderProgram();
    m_drawShadowMapTexProg->attachShader(&shadowMapTexVert);
    m_drawShadowMapTexProg->attachShader(&shadowMapTexFrag);
    m_drawShadowMapTexProg->link();
}

void
GLWidget::delShaderProg(ShaderProgram *shaderProg)
{
    if (shaderProg != nullptr) {
        shaderProg->detachAll();
        delete shaderProg;
    }
}

void
GLWidget::delAllShaderProgs()
{
    delShaderProg(m_hmapDiffuseProg);
    delShaderProg(m_hmapNormalProg);
    delShaderProg(m_causticsCalcProg);
    delShaderProg(m_hmapPosRenderProg);
    delShaderProg(m_hmapNormalRenderProg);
    delShaderProg(m_screenQuadProg);
    delShaderProg(m_poolProg);
    delShaderProg(m_sphereProg);
    delShaderProg(m_waterSurfaceProg);
    delShaderProg(m_drawShadowMapTexProg);
}

void
GLWidget::refreshShaderProg()
{
    delAllShaderProgs();
    genShaderProg();
}

void
GLWidget::diffuseHmap()
{
    glBindFramebuffer(GL_FRAMEBUFFER, m_hmapDiffuseFBO);
    // std::cout << glErrorToString(glCheckFramebufferStatus(GL_FRAMEBUFFER)) << std::endl;
    glViewport(0, 0, m_hmapSize, m_hmapSize);
    m_hmapDiffuseProg->useProgram();
    glBindVertexArray(m_hmapVao);
    m_writeTexIdx = 1 - m_writeTexIdx;
    if (m_writeTexIdx == 0) {;
        GLenum draw_buffers[] = {GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT2};
        glDrawBuffers(2, draw_buffers);
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, m_hmapPosTex[1]);
        glActiveTexture(GL_TEXTURE1);
        glBindTexture(GL_TEXTURE_2D, m_hmapVelocityTex[1]);
    } else {
        GLenum draw_buffers[] = {GL_COLOR_ATTACHMENT1, GL_COLOR_ATTACHMENT3};
        glDrawBuffers(2, draw_buffers);
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, m_hmapPosTex[0]);
        glActiveTexture(GL_TEXTURE1);
        glBindTexture(GL_TEXTURE_2D, m_hmapVelocityTex[0]);
    }
    glClearColor(0, 0, 0, 1);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    GLfloat deltaT = INTERVAL / 1000.f;
    GLfloat columnWidth = m_waterSurface->edgeLength() / static_cast<GLfloat>(m_hmap->size());
    GLfloat maxWaveSpeed = columnWidth / deltaT;
    GLfloat waveSpeed = std::min(maxWaveSpeed * 0.8f, 0.65f);
    GLfloat cOVERh_squared = std::pow(waveSpeed / deltaT, 2.0f);
    GLfloat fMax = 2048.f;
    GLfloat velocityDampFactor = 0.99f;
    GLfloat coSphereWave = 0.07f;
    GLfloat coRipple = 0.85f;
    glUniform1f((*m_hmapDiffuseProg)["deltaT"], deltaT);
    glUniform1f((*m_hmapDiffuseProg)["cOVERh_squared"], cOVERh_squared);
    glUniform1i((*m_hmapDiffuseProg)["hmapSize"], m_hmap->size());
    glUniform1f((*m_hmapDiffuseProg)["fMax"], fMax);
    glUniform1f((*m_hmapDiffuseProg)["velocityDampFactor"], velocityDampFactor);
    glUniform1f((*m_hmapDiffuseProg)["coSphereWave"], coSphereWave);
    glUniform1f((*m_hmapDiffuseProg)["coRipple"], coRipple);
    vec2i rippleILoc = m_waterSurface->texelIPos(m_rippleWorldPos);
    glUniform2i((*m_hmapDiffuseProg)["rippleILoc"], rippleILoc.s, rippleILoc.t);

    glUniform1f((*m_hmapDiffuseProg)["worldEdgeLen"], m_waterSurface->edgeLength());
    GLfloat vec3Data[3];
    getVec3DData(m_sphere->translation(), vec3Data);
    glUniform3fv((*m_hmapDiffuseProg)["spherePos"], 1, vec3Data);

    getVec3DData(m_sphereLastPos, vec3Data);
    glUniform3fv((*m_hmapDiffuseProg)["lastSpherePos"], 1, vec3Data);
    glUniform1f((*m_hmapDiffuseProg)["spehreRadius"], m_sphere->radius());
    glUniform1f((*m_hmapDiffuseProg)["waterSurfaceH"], m_waterSurface->height());

    glDrawArrays(GL_POINTS, 0, m_hmap->numVertices());
    m_rippleWorldPos = QVector3D(100.f, 100.f, 100.f);
    catchGLError("diffuseHmap->end");
    // m_paused = true;
}

void
GLWidget::calcHmapNormal()
{
    glBindFramebuffer(GL_FRAMEBUFFER, m_hmapDiffuseFBO);
    GLenum draw_buffers[] = {GL_COLOR_ATTACHMENT4} ;
    glDrawBuffers(1, draw_buffers);
    glViewport(0, 0, m_hmapSize, m_hmapSize);
    glClearColor(0, 0, 0, 1);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    m_hmapNormalProg->useProgram();
    glBindVertexArray(m_hmapVao);
    glUniform1i((*m_hmapNormalProg)["hmapSize"], m_hmapSize);
    glUniform1f((*m_hmapNormalProg)["worldEdgeLen"], m_waterSurface->edgeLength());
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, m_hmapPosTex[m_writeTexIdx]);
    glDrawArrays(GL_POINTS, 0, m_hmap->numVertices());
    catchGLError("calcHmapNormal->end");
}

void
GLWidget::calcCaustics()
{
    glBindFramebuffer(GL_FRAMEBUFFER, m_hmapDiffuseFBO);
    GLenum draw_buffers[] = {GL_COLOR_ATTACHMENT5} ;
    glDrawBuffers(1, draw_buffers);
    glViewport(0, 0, m_hmapSize, m_hmapSize);
    glClearColor(0, 0, 0, 1);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    m_causticsCalcProg->useProgram();
    GLfloat coCausticsDF = 1.0f;
    GLfloat deltaCausticsDF = 0.005f;
    GLfloat maxCausticsRatio = 0.8f;
    glBindVertexArray(m_hmapVao);
    glUniform1f((*m_causticsCalcProg)["coCausticsDF"], coCausticsDF);
    glUniform1f((*m_causticsCalcProg)["deltaCausticsDF"], deltaCausticsDF);
    glUniform1f((*m_causticsCalcProg)["maxCausticsRatio"], maxCausticsRatio);
    glUniform1i((*m_causticsCalcProg)["hmapSize"], m_hmapSize);
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, m_hmapPosTex[1 - m_writeTexIdx]);
    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_2D, m_hmapPosTex[m_writeTexIdx]);
    // glDrawArrays(GL_POINTS, 0, m_hmap->numVertices());
    glDrawElements(GL_TRIANGLES, m_hmap->numIndices(), GL_UNSIGNED_INT, 0);
    catchGLError("calcCaustics->end");
}

void
GLWidget::genShadowMap()
{
    glBindFramebuffer(GL_FRAMEBUFFER, m_shadowMapFBO);
    catchGLError("genShadowMap->bind");
    glDrawBuffer(GL_NONE);
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);
    catchGLError("genShadowMap->state setup 1");
    glViewport(0, 0, m_shadowMapSize, m_shadowMapSize);
    glClearColor(0, 0, 0, 1);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    catchGLError("genShadowMap->state setup 2");
    drawPool(true);
    catchGLError("genShadowMap->drawPool");
    drawSphere(true);
    catchGLError("genShadowMap->drawSphere");
}

void
GLWidget::setupSmallViewport()
{
    int len = std::min(height(), width()) / 2;
    glViewport(0, 0, len, len);
}

void
GLWidget::testHmapMesh()
{
    glClearColor(0, 0, 0, 1);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    setupSmallViewport();
    m_causticsCalcProg->useProgram();
    glBindVertexArray(m_hmapVao);
    glUniform1i((*m_causticsCalcProg)["hmapSize"], m_hmapSize);
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, m_hmapPosTex[1 - m_writeTexIdx]);
    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_2D, m_hmapPosTex[m_writeTexIdx]);
    glDrawElements(GL_TRIANGLES, m_hmap->numIndices(), GL_UNSIGNED_INT, 0);
    catchGLError("testHmapMesh->END");
}

void
GLWidget::drawHmapNormalTex()
{
    setupSmallViewport();
    glBindVertexArray(m_screenQuadVao);
    m_hmapNormalRenderProg->useProgram();
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, m_hmapNormalTex);
    glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
}

void
GLWidget::drawHmapPositionTex()
{
    setupSmallViewport();
    glBindVertexArray(m_screenQuadVao);
    m_hmapPosRenderProg->useProgram();
    GLfloat posFactor = 10.f;
    glUniform1f((*m_hmapPosRenderProg)["posFactor"], posFactor);
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, m_hmapPosTex[m_writeTexIdx]);
    glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
}

void
GLWidget::drawCausticsTex()
{
    setupSmallViewport();
    glBindVertexArray(m_screenQuadVao);
    m_hmapPosRenderProg->useProgram();
    GLfloat posFactor = 10.f;
    glUniform1f((*m_hmapPosRenderProg)["posFactor"], posFactor);
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, m_causticsTex);
    glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
}

void
GLWidget::drawHmapVelocityTex()
{
    setupSmallViewport();
    glBindVertexArray(m_screenQuadVao);
    m_hmapPosRenderProg->useProgram();
    GLfloat posFactor = 5.f;
    glUniform1f((*m_hmapPosRenderProg)["posFactor"], posFactor);
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, m_hmapVelocityTex[m_writeTexIdx]);
    glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
}

void
GLWidget::drawShadowMapTex()
{
    setupSmallViewport();
    glBindVertexArray(m_screenQuadVao);
    m_drawShadowMapTexProg->useProgram();
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, m_shadowMapTex);
    glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
    catchGLError("drawShadowMapTex->END");
}

QMatrix4x4
GLWidget::VP(bool inLightSpace)
{
    QMatrix4x4 viewMat;
    QMatrix4x4 projMat;
    if (inLightSpace) {
        viewMat.setToIdentity();
        viewMat.lookAt(m_lightPos, m_lightDir + m_lightPos, QVector3D(0.f, 1.f, 0.f));
        PerspectiveProj lightPersp(m_shadowMapSize, m_shadowMapSize);
        lightPersp.setVerticalAngle(50);
        projMat = lightPersp.projMat();
    } else {
        viewMat = m_cam->viewMat();
        projMat = m_persp->projMat();
    }
    return projMat * viewMat;
}

void
GLWidget::setupScreenUniforms()
{
    ShaderProgram *prog = m_screenQuadProg;
    GLfloat vec3Data[3];
    GLfloat mat4Data[16];
    QMatrix4x4 biasMat(0.5f, 0.0f, 0.0f, 0.0f,
                       0.0f, 0.5f, 0.0f, 0.0f,
                       0.0f, 0.0f, 0.5f, 0.0f,
                       0.5f, 0.5f, 0.5f, 1.0f
                );
    QMatrix4x4 biasVP = VP(true);
    getVec3DData(m_cam->eye(), vec3Data);
    glUniform3fv((*prog)["camPos"], 1, vec3Data);

    getVec3DData(m_lightPos, vec3Data);
    glUniform3fv((*prog)["lightPos"], 1, vec3Data);
    getVec3DData(m_lightDir, vec3Data);
    glUniform3fv((*prog)["lightDir"], 1, vec3Data);
    glUniform1f((*prog)["lightPlaneH"], m_lightPos.y() - m_DistlightPos2Plane);
    getVec3DData(m_ILMax, vec3Data);
    glUniform3fv((*prog)["lightIMax"], 1, vec3Data);
    glUniform1f((*prog)["lightExp"], m_sExp);
    glUniform1f((*prog)["cosTp"], std::cos(m_penumbraAngle));
    glUniform1f((*prog)["cosTu"], std::cos(m_umbraAngle));

    getVec3DData(m_sphere->translation(), vec3Data);
    glUniform3fv((*prog)["sphereCenter"], 1, vec3Data);
    glUniform1f((*prog)["sphereRadius"], m_sphere->radius());
    get3fvData(m_sphere->kd(), vec3Data);
    glUniform3fv((*prog)["sphereKd"], 1, vec3Data);
    glUniform1f((*prog)["sphereID"], static_cast<GLfloat>(m_sphere->pickColor()) / 255.f);

    glUniform1f((*prog)["poolEdgeLen"], m_pool->edgeLength());
    glUniform1f((*prog)["poolHeight"], m_pool->height());
    glUniform1f((*prog)["tileTexWorldLen"], m_pool->texLength());
    glUniform1i((*prog)["tileTexSize"], m_tileTexSize);
    glUniform1f((*prog)["poolID"], static_cast<GLfloat>(m_pool->pickColor()) / 255.f);

    glUniform1f((*prog)["waterSurfaceH"], m_waterSurface->height());
    glUniform1i((*prog)["hmapSize"], m_waterSurface->hmapSize());
    glUniform1f((*prog)["waterSurfaceID"], static_cast<GLfloat>(m_waterSurface->pickColor()) / 255.f);
    glUniform1f((*prog)["shadowMapSize"], m_shadowMapSize);

    getMat4x4Data(biasVP, mat4Data);
    glUniformMatrix4fv((*prog)["shadowBiasVP"], 1, GL_TRUE, mat4Data);

    for (int i = 0; i < 4; ++i) {
        glActiveTexture(GL_TEXTURE0 + i);
        glBindTexture(GL_TEXTURE_2D, m_pickingTexes[i]);
    }
    glActiveTexture(GL_TEXTURE4);
    glBindTexture(GL_TEXTURE_2D, m_tileTex);
    glActiveTexture(GL_TEXTURE5);
    glBindTexture(GL_TEXTURE_2D, m_shadowMapTex);
    glActiveTexture(GL_TEXTURE6);
    glBindTexture(GL_TEXTURE_2D, m_hmapPosTex[m_writeTexIdx]);
    glActiveTexture(GL_TEXTURE7);
    glBindTexture(GL_TEXTURE_2D, m_hmapNormalTex);
    glActiveTexture(GL_TEXTURE8);
    glBindTexture(GL_TEXTURE_2D, m_causticsTex);
    catchGLError("setupScreenUniforms->END");

}

void
GLWidget::drawPool(bool inLightSpace)
{
    GLfloat mat4Data[16];
    m_poolProg->useProgram();

    getMat4x4Data(VP(inLightSpace), mat4Data);
    glUniformMatrix4fv((*m_poolProg)["VP"], 1, GL_TRUE, mat4Data);
    glUniform1ui((*m_poolProg)["pickingID"], m_pool->pickColor());
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, m_tileTex);

    glBindVertexArray(m_poolVao);
    glDrawArrays(GL_TRIANGLES, 0, m_pool->numVertices());
}

void
GLWidget::drawSphere(bool inLightSpace)
{
    GLfloat mat4Data[16];
    GLfloat vec3Data[3];
    m_sphereProg->useProgram();

    getMat4x4Data(VP(inLightSpace), mat4Data);
    glUniformMatrix4fv((*m_sphereProg)["VP"], 1, GL_TRUE, mat4Data);
    glUniform1ui((*m_sphereProg)["pickingID"], m_sphere->pickColor());
    getVec3DData(m_sphere->translation(), vec3Data);
    glUniform3fv((*m_sphereProg)["sphereCenter"], 1, vec3Data);
    glUniform1f((*m_sphereProg)["sphereRadius"], m_sphere->radius());

    glBindVertexArray(m_sphereVao);
    GLfloat tessLvl = m_sphere->tessLevel();
    GLfloat outerLevel[4] = {tessLvl, tessLvl, tessLvl, tessLvl};
    GLfloat innerLevel[2] = {tessLvl, tessLvl};
    glPatchParameterfv(GL_PATCH_DEFAULT_OUTER_LEVEL, outerLevel);
    glPatchParameterfv(GL_PATCH_DEFAULT_INNER_LEVEL, innerLevel);
    glPatchParameteri(GL_PATCH_VERTICES, 3);

    glDrawElements(GL_PATCHES, m_sphere->numIndices(), GL_UNSIGNED_BYTE, 0);
}

void
GLWidget::drawWaterSurface()
{
    GLfloat mat4Data[16];
    m_waterSurfaceProg->useProgram();

    glUniform1f((*m_waterSurfaceProg)["waterSurfaceH"], m_waterSurface->height());
    getMat4x4Data(VP(), mat4Data);
    glUniformMatrix4fv((*m_waterSurfaceProg)["VP"], 1, GL_TRUE, mat4Data);
    glUniform1ui((*m_waterSurfaceProg)["pickingID"], m_waterSurface->pickColor());
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, m_hmapPosTex[m_writeTexIdx]);
    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_2D, m_hmapNormalTex);

    glBindVertexArray(m_waterSurfaceVao);
    glPatchParameteri(GL_PATCH_VERTICES, 4);
    glDrawElements(GL_PATCHES, m_waterSurface->numIndices(), GL_UNSIGNED_INT, 0);
}

void
GLWidget::drawScene()
{
    glBindFramebuffer(GL_FRAMEBUFFER, m_pickingFBO);

    GLenum draw_buffers[] = {GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1,
                             GL_COLOR_ATTACHMENT2, GL_COLOR_ATTACHMENT3};

    glEnable(GL_CULL_FACE);
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

    if (m_wireFrameOn) {
        glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    } else {
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    }

    glDrawBuffers(4, draw_buffers);
    glClearColor(0, 0, 0, 1);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glDepthRange(0, 0.01);
    switch(m_contentIdx) {
    case HM_NOR_TEX:
        drawHmapNormalTex();
        break;
    case HM_POS_TEX:
        drawHmapPositionTex();
        break;
    case HM_VEL_TEX:
        drawHmapVelocityTex();
        break;
    case CAUSTICS_TEX:
        drawCausticsTex();
        break;
    case HM_MESH_TEST:
        testHmapMesh();
        break;
    case SHADOW_MAP_TEX:
        drawShadowMapTex();
        break;
    default:
        break;
    }

    glViewport(0, 0, width(), height());

    glDepthRange(0.01, 1.0);
    glDisable(GL_CULL_FACE);
    drawWaterSurface();
    glEnable(GL_CULL_FACE);
    glCullFace(GL_BACK);
    drawPool();
    drawSphere();
    glDepthRange(0, 1.0);

    glReadBuffer(GL_COLOR_ATTACHMENT1);
    glBindBuffer(GL_PIXEL_PACK_BUFFER, m_pickingPpBO);
    glPixelStorei(GL_PACK_ALIGNMENT, 1);
    glBufferData(GL_PIXEL_PACK_BUFFER, width() * height() * sizeof(GLubyte), 0, GL_STREAM_READ);
    glReadPixels(0, 0, width(), height(), GL_RED, GL_UNSIGNED_BYTE, 0);
    glBindBuffer(GL_PIXEL_PACK_BUFFER, 0);

    glReadBuffer(GL_COLOR_ATTACHMENT2);
    glBindBuffer(GL_PIXEL_PACK_BUFFER, m_worldPosPpBo);
    glPixelStorei(GL_PACK_ALIGNMENT, 4);
    glBufferData(GL_PIXEL_PACK_BUFFER, width() * height() * 3 * sizeof(GLfloat), 0, GL_STREAM_READ);
    glReadPixels(0, 0, width(), height(), GL_RGB, GL_FLOAT, 0);
    glBindBuffer(GL_PIXEL_PACK_BUFFER, 0);

    glReadBuffer(GL_COLOR_ATTACHMENT3);
    glBindBuffer(GL_PIXEL_PACK_BUFFER, m_normalPpBo);
    glPixelStorei(GL_PACK_ALIGNMENT, 4);
    glBufferData(GL_PIXEL_PACK_BUFFER, width() * height() * 3 * sizeof(GLfloat), 0, GL_STREAM_READ);
    glReadPixels(0, 0, width(), height(), GL_RGB, GL_FLOAT, 0);
    glBindBuffer(GL_PIXEL_PACK_BUFFER, 0);
}

GLubyte
GLWidget::pick(const QPoint &pos2D)
{
    GLubyte id = pickingID(GeomType::UNKNOWN);
    if (pos2D.x() < 0 || pos2D.y() < 0 || pos2D.x() >= width() || pos2D.y() >= height()) {
        return id;
    }

    glBindBuffer(GL_PIXEL_PACK_BUFFER, m_pickingPpBO);
    GLubyte *pickingIDs = (GLubyte*)glMapBuffer(GL_PIXEL_PACK_BUFFER, GL_READ_ONLY);

    if (pickingIDs) {
        id = pickingIDs[pos2D.y() * width() + pos2D.x()];
    }
    glUnmapBuffer(GL_PIXEL_PACK_BUFFER);
    return id;
}
void
GLWidget::locationNormal(const QPoint &pos2D)
{
    glBindBuffer(GL_PIXEL_PACK_BUFFER, m_normalPpBo);
    GLfloat* normals = (GLfloat*)glMapBuffer(GL_PIXEL_PACK_BUFFER, GL_READ_ONLY);
    QVector3D normal;
    if (normals) {
        int idxX = 3 * (pos2D.y() * width() + pos2D.x());
        int idxY = idxX + 1;
        int idxZ = idxY + 1;
        normal = QVector3D(normals[idxX], normals[idxY], normals[idxZ]);
    }
    glUnmapBuffer(GL_PIXEL_PACK_BUFFER);
    std::cout << "click normal = " << normal << std::endl;
}

void
GLWidget::locateRipple(const QPoint &pos2D)
{
    glBindBuffer(GL_PIXEL_PACK_BUFFER, m_worldPosPpBo);
    GLfloat* locations = (GLfloat*)glMapBuffer(GL_PIXEL_PACK_BUFFER, GL_READ_ONLY);
    QVector3D location;
    if (locations) {
        int idxX = 3 * (pos2D.y() * width() + pos2D.x());
        int idxY = idxX + 1;
        int idxZ = idxY + 1;
        location = QVector3D(locations[idxX], locations[idxY], locations[idxZ]);
    }
    glUnmapBuffer(GL_PIXEL_PACK_BUFFER);
    std::cout << "click pos = " << location << std::endl;
    if (m_pickingID == m_waterSurface->pickColor()) {
        m_rippleWorldPos = location;
    }
}

void
GLWidget::drawScreenQuad()
{
    QOpenGLFramebufferObject::bindDefault();
    // glBindFramebuffer(GL_FRAMEBUFFER, 0);
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    glClearColor(0.f, 0.f, 0.f, 1.f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glBindVertexArray(m_screenQuadVao);
    m_screenQuadProg->useProgram();
    setupScreenUniforms();
    catchGLError("drawScreenQuad->setupScreenUniforms");
    glViewport(0, 0, width(), height());
    glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
}

void
GLWidget::releaseGL()
{
    glDeleteBuffers(3, m_waterSurfaceBos);
    glDeleteVertexArrays(1, &m_waterSurfaceVao);
    delete m_waterSurface;

    glDeleteBuffers(2, m_hmapBos);
    glDeleteVertexArrays(1, &m_hmapVao);
    delete m_hmap;

    glDeleteTextures(2, m_sphereBos);
    glDeleteVertexArrays(1, &m_sphereVao);
    delete m_sphere;

    glDeleteTextures(1, &m_tileTex);
    glDeleteBuffers(3, m_poolBos);
    glDeleteVertexArrays(1, &m_poolVao);
    delete m_pool;

    glDeleteBuffers(2, m_screenQuadBos);
    glDeleteVertexArrays(1, &m_screenQuadVao);

    glDeleteBuffers(1, &m_worldPosPpBo);
    glDeleteBuffers(1, &m_pickingPpBO);
    glDeleteBuffers(1, &m_normalPpBo);
    glDeleteTextures(4, m_pickingTexes);
    glDeleteFramebuffers(1, &m_pickingFBO);
    glDeleteFramebuffers(1, &m_hmapDiffuseFBO);
    glDeleteTextures(1, &m_shadowMapTex);
    glDeleteFramebuffers(1, &m_shadowMapFBO);

    delAllShaderProgs();
}

GLWidget::~GLWidget()
{
    releaseGL();
    delete m_timer;
    delete m_persp;
    delete m_cam;
    delete [] m_initTexPos;
    delete [] m_initTexVel;
}
