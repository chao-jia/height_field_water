#include "mainwindow.h"

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent)
{
    m_widget = new GLWidget(this);
    setWindowTitle("Water");
    setFocusPolicy(Qt::NoFocus);
    setCentralWidget(m_widget);
    setupStatusBar();
    connect(m_widget, SIGNAL(FPSChanged(float)),
            this, SLOT(updateFPSLabel(float)));
}

void
MainWindow::setupStatusBar()
{
    QStatusBar *statusBar = new QStatusBar();
    m_fpsLabel = new QLabel("0");
    statusBar->addWidget(m_fpsLabel);
    //setStatusBar(statusBar);
}

void
MainWindow::updateFPSLabel(float fps)
{
    m_fpsLabel->setText(QString("%1").arg(fps));
}
