#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QtWidgets>
#include "glwidget.h"


class MainWindow : public QMainWindow
{
    Q_OBJECT
private:
    GLWidget* m_widget;
    QLabel * m_fpsLabel;
public:
    explicit MainWindow(QWidget *parent = 0);
    void setupStatusBar();
signals:

public slots:
    void updateFPSLabel(float fps);
};

#endif // MAINWINDOW_H
