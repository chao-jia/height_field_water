#ifndef CAMERA_H
#define CAMERA_H

#include "miscellaneous.h"
#include "templates.h"

//====================================== PerspectiveProj class ==================================== //
// Perspective Projection
class PerspectiveProj {
private:
    float m_verticalAngle;
    float m_aspectRatio;
    float m_nearPlane;
    float m_farPlane;
public:
    PerspectiveProj (int width, int height);
    PerspectiveProj (float verticalAngle, float aspectRatio, float nearPlane, float farPlane);
    PerspectiveProj (float verticalAngle, int width, int height, float nearPlane, float farPlane);
    ~PerspectiveProj() {}
    void setAspectRatio(int width, int height);
    void setVerticalAngle(float angle) {m_verticalAngle = angle;}
    // delta > 0 ==> zoom in; delta < 0 ==> zoom out
    void zoom(float delta);
    void reset();
    QMatrix4x4 projMat() const;
};

//========================================= Cam class ============================================= //
class Cam
{
private:
    QVector3D m_poi;
    QVector3D m_pos;
    QVector3D m_up;
    QVector3D m_initPoi;
    QVector3D m_initPos;
public:
    Cam();
    Cam(const QVector3D &_initPoi, const QVector3D &_initPos);
    ~Cam() {}
    void reset();
    QVector3D u() const;
    QVector3D v() const;
    QVector3D w() const;

    QMatrix4x4 viewMat() const;
    QMatrix4x4 invertedViewMat() const;
    QVector3D poi() const {return m_poi;}
    QVector3D lookAt() const { return -w();}
    // camera position in world space
    QVector3D eye() const { return m_pos;}
    void orbit(QPointF rotationCenter, QPointF lastPos, QPointF currentPos);
    void rotate(QPointF rotationCenter, QPointF lastPos, QPointF currentPos);
};

#endif // CAMERA_H
