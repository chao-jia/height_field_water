# Heightfield-based Real-Time Water Simulation and Rendering

Inspired by [WebGL Water](https://madebyevan.com/webgl-water/), featuring ray-traced reflections and refractions, soft shadow and caustics. The scene is very simple, thus ray tracing in fragment shader for reflection and refraction is relatively cheap.
The water simulation is heightfield based[^1], which is very efficient. The program can run smoothly on low-end GPU (e.g. NVIDIA GeForce GT 630M).

As a freely chosen topic for the final assignment of a practicum course in 2016 summer semester, originally I implemented it with C++, Qt 5 and OpenGL 4 using Qt Creator. Recently switched to CMake build system and Qt 6. The backward compatibility of Qt is surprisingly good. The code still compiles and runs smoothly almost without any change. 

Video on YouTube:

[![](https://img.youtube.com/vi/xU9GIU0JsA0/0.jpg)](https://www.youtube.com/watch?v=xU9GIU0JsA0 "Video on YouTube")

[^1]: [Fast Water Simulation for Games Using Height Fields](https://www.gdcvault.com/play/203/Fast-Water-Simulation-for-Games)
