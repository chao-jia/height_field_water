import os, shutil, json
import sys
import subprocess
import argparse

def clean_dir(wd):
    if not os.path.isdir(wd):
        print("######## {} does not exist ########".format(wd))
        return
        
    print("######## removing all contents of {} ########".format(wd))
    for file_i in os.listdir(wd):
        path_i = os.path.join(wd, file_i)
        try:
            if os.path.isfile(path_i):
                os.unlink(path_i)
            elif os.path.isdir(path_i): 
                shutil.rmtree(path_i)
        except Exception as e:
            print(e)
            exit(1)


parser = argparse.ArgumentParser(description='build and compile')
parser.add_argument('--force', action='store_true', required=False, help='remove all the build files, then rebuild and recompile.')
parser.add_argument('--conf', action='store_true', required=False, help='cmake configure only')
parser.add_argument('--release', action='store_true', required=False, help='only build release version')
parser.add_argument('--clean', action='store_true', required=False, help='remove binaries')

args = parser.parse_args()

arg_force = True if args.force else False
arg_conf = True if args.conf else False
arg_release = True if args.release else False
arg_clean = True if args.clean else False

cwd = os.getcwd()
build_dir = cwd + os.sep + 'build'
release_dir = cwd + os.sep + 'release'
debug_dir = cwd + os.sep + 'debug'

if arg_clean:
    if os.path.isdir(debug_dir):
        shutil.rmtree(debug_dir)
    if os.path.isdir(release_dir):
        shutil.rmtree(release_dir)
    if os.path.isdir(build_dir):
        shutil.rmtree(build_dir)
    exit(0)

if not os.path.isdir(build_dir):
    os.makedirs(build_dir)

if arg_force:
    clean_dir(build_dir)

cmake_generator = ""
vcpkg_root = os.environ["VCPKG_ROOT"]
vcpkg_triplet = os.getenv("VCPKG_DEFAULT_TRIPLET", "")

runcmake_json = os.path.join(cwd, '.runcmake')
if os.path.isfile(runcmake_json):
    try:    
        with open(runcmake_json) as runcmake_json_file:
            runcmake_config = json.load(runcmake_json_file)
            if "vs_version" in runcmake_config:
                cmake_generator = f'-G "Visual Studio {runcmake_config["vs_version"]}" -A x64'
            if "vcpkg_triplet" in runcmake_config:
                vcpkg_triplet = runcmake_config["vcpkg_triplet"]
    except Exception as e:
        print(type(e))
        print(e)
        exit(1)

config_cmd = f'cmake -DCMAKE_TOOLCHAIN_FILE={vcpkg_root}/scripts/buildsystems/vcpkg.cmake -DVCPKG_TARGET_TRIPLET={vcpkg_triplet} {cmake_generator} ..'
build_debug_cmd = 'cmake --build . --config Debug'
build_release_cmd = 'cmake --build . --config Release'

print(f"######## config: {config_cmd} ########")
subprocess.run(config_cmd, cwd=build_dir)

if arg_conf:
    print("######## config done ########")
    exit()

if not arg_release:
    print("######## build debug ########")
    subprocess.run(build_debug_cmd, cwd=build_dir)

print("######## build release ########")
subprocess.run(build_release_cmd, cwd=build_dir)



