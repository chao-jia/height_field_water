#version 430 core

layout (binding = 0) uniform sampler2D normalTex;

in vec2 vsTexCoord;

layout (location = 0) out vec4 color;
layout (location = 1) out vec4 pickColor;
layout (location = 2) out vec3 worldPos;

vec4 colorFromNormal()
{
    vec3 color = vec3(0.0);
    vec3 texel = texture(normalTex, vsTexCoord).rgb;
    color = normalize(texel);
    color = 5 * abs(color);
    color.g = 0.1;
    return vec4(color, 1.0);
    // return vec4(clamp(texel.r, 0, 1), clamp(texel.g, 0, 1), clamp(texel.b, 0, 1), 1);
}

void main(void)
{
    color = colorFromNormal();
    // color = vec4(0.0, 0.0, 1.0, 1.0);
    pickColor = vec4(0, 0, 0, 1);
    worldPos = texture(normalTex, vsTexCoord).rgb;
}
