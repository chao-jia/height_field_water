#version 430 core
in float posNew;
in float velNew;
in float posOld;

layout (location = 0) out vec4 posTexel;
layout (location = 1) out vec4 velTexel;

void main(void)
{
    posTexel = vec4(posNew, 0.0, 0.0, 1.0);
    velTexel = vec4(velNew, 0.0, 0.0, 1.0);
}
