#version 430 core

in float oldPos;
in float newPos;
uniform float coCausticsDF; // 1.0
uniform float deltaCausticsDF; // 0.003
uniform float maxCausticsRatio; // 0.8
layout (location = 0) out vec4 causticsTexel;

void main(void)
{
    float causticsFactor = 4.0;
    float epsilon = 0.036;
    float xOld = coCausticsDF * oldPos;
    float xNew = coCausticsDF * newPos;
    float oldArea = length(dFdx(xOld) + deltaCausticsDF) * length(dFdy(xOld) + deltaCausticsDF);
    float newArea = length(dFdx(xNew) + deltaCausticsDF) * length(dFdy(xNew) + deltaCausticsDF);
    float ratio = (newArea/oldArea - 1.0);
    ratio = clamp(ratio, -maxCausticsRatio, maxCausticsRatio);
    causticsTexel = vec4(ratio, 0.0, 0.0, 1.0);
    // causticsTexel = vec4(1, 0.0, 0.0, 1.0);
}
