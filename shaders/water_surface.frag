#version 430 core
uniform uint pickingID;

in vec3 teNormal;
in vec3 teWorldPos;

layout (location = 0) out vec4 color;
layout (location = 1) out vec4 pickColor;
layout (location = 2) out vec3 worldPos;
layout (location = 3) out vec3 normal;

void main(void)
{
    vec3 c = (teNormal + vec3(1.0)) / 2.0;
    color = vec4(c, 1.0);
    pickColor = vec4(float(pickingID) / 255.0, 0.0, 0.0, 1.0);
    worldPos = teWorldPos;
    normal = teNormal;
}
