#version 430 core

uniform float posFactor;
layout (binding = 0) uniform sampler2D posTex;

in vec2 vsTexCoord;

layout (location = 0) out vec4 color;
layout (location = 1) out vec4 pickColor;
layout (location = 2) out vec3 worldPos;

vec4 colorFromPos()
{
    float texel = clamp(posFactor * texture(posTex, vsTexCoord).r, -1.0, 1.0);
    vec4 c;
    if (texel > 0.0) {
        c = vec4(texel, 0.0, 0.0, 1.0);
    } else {
        c = vec4(0.0, 0.0, -texel, 1.0);
    }
    return c;
}

void main(void)
{
    color = colorFromPos();
    pickColor = vec4(0, 0, 0, 1);
    worldPos = vec3(texture(posTex, vsTexCoord).r, 0, 0);
}
