#version 430 core
in vec3 normal;
layout (location = 0) out vec4 normalTexel;
void main(void)
{
    normalTexel = vec4(normal, 1.0);
    // normalTexel = vec4(0.0, 0.0, 1.0, 1.0);
}
