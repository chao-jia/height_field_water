#version 430 core

uniform mat4 VP;

layout(location = 0) in vec3 position;
layout(location = 1) in vec3 normal;
layout(location = 2) in vec2 texCoord;

out vec3 vWorldPos;
out vec3 vWorldNormal;
out vec2 vTexCoord;

void main(void)
{
    vWorldPos = position;
    gl_Position = VP * vec4(vWorldPos, 1.0);
    vWorldNormal = normal;
    vTexCoord = texCoord;
}
