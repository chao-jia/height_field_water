#version 430 core

uniform vec3 camPos;
uniform vec3 lightPos;
uniform vec3 lightDir; // from light to obj
uniform float lightPlaneH;
uniform vec3 lightIMax;
uniform float lightExp;
uniform float cosTp; // penumbraAngle
uniform float cosTu; // umbraAngle

uniform vec3 sphereCenter;
uniform float sphereRadius;
uniform vec3 sphereKd;
uniform float sphereID; // normalized

uniform float poolEdgeLen;
uniform float poolHeight;
uniform float tileTexWorldLen;
uniform int tileTexSize;
uniform float poolID; // normalized

uniform float waterSurfaceH;
uniform int hmapSize;
uniform float waterSurfaceID; // normalized

uniform int numSampledRays; // == 1
uniform int recursionDepth;
uniform float randVecTexelSize;

uniform float shadowMapSize;
uniform mat4 shadowBiasVP;

layout (binding = 0) uniform sampler2D sceneryTex;
layout (binding = 1) uniform sampler2D pickIDTex;
layout (binding = 2) uniform sampler2D worldPosTex;
layout (binding = 3) uniform sampler2D normalTex;
layout (binding = 4) uniform sampler2D tileTex;
layout (binding = 5) uniform sampler2D shadowMapTex;
layout (binding = 6) uniform sampler2D waterPosTex;
layout (binding = 7) uniform sampler2D waterNormalTex;
layout (binding = 8) uniform sampler2D causticsTex;

in vec2 vsTexCoord;
out vec4 color;

const vec3 waterKd = vec3(0.6, 0.9, 1.0);
const vec3 AMBIENT = vec3(0.55);
const float FLOAT_MAX = 1.0 / 0.0;
const float LIGHT_ID = 1.0;
const float n_water = 1.333; // refractive index of water
const float M_PI = 3.141592653;
const float minAlpha = 0.2; // for soft shadow

struct Ray {
    vec3 o;
    vec3 dir;
};

bool eq(float x, float y)
{
    float EPSILON = 0.00001;
    return abs(x - y) < EPSILON;
}

bool isWaterSurfacePixel(float id)
{
    return eq(id, waterSurfaceID);
}

bool isPoolPixel(float id)
{
    return eq(id, poolID);
}

bool isSpherePixel(float id)
{
    return eq(id, sphereID);
}

vec3 poolKd(vec3 pos)
{
    bool invalidPos = false;
    vec2 texCoord = vec2(0.0, 0.0);
    if (eq(pos.x, -poolEdgeLen/2.0)) { // left
        texCoord = pos.yz;
        texCoord += vec2(poolHeight/2.0, poolEdgeLen / 2.0);
        texCoord /= tileTexWorldLen;
    } else if (eq(pos.z, -poolEdgeLen/2.0)) { // back
        texCoord = pos.xy;
        texCoord += vec2(poolEdgeLen / 2.0, poolHeight/2.0);
        texCoord /= tileTexWorldLen;
    } else if (eq(pos.x, poolEdgeLen/2.0)) { // right
        texCoord = pos.yz;
        texCoord += vec2(poolHeight/2.0, poolEdgeLen / 2.0);
        texCoord /= tileTexWorldLen;
    } else if (eq(pos.z, poolEdgeLen/2.0)) { // front
        texCoord = pos.xy;
        texCoord += vec2(poolEdgeLen / 2.0, poolHeight/2.0);
        texCoord /= tileTexWorldLen;
    } else if (eq(pos.y, -poolHeight/2.0)) { // bottom
        texCoord = pos.zx;
        texCoord += vec2(poolEdgeLen / 2.0, poolEdgeLen/2.0);
        texCoord /= tileTexWorldLen;
    } else {
        invalidPos = true;
    }
    if (invalidPos) return vec3(0.0, 0.0, 0.0);
    return 0.6 * texture(tileTex, texCoord).rgb;
}

vec3 poolNormal(vec3 pos)
{
    float maxX = poolEdgeLen / 2.0;
    float maxY = poolHeight / 2.0;
    float maxZ =  poolEdgeLen / 2.0;
    if (eq(pos.x, -maxX)) {
        return vec3(1, 0, 0);
    } else if (eq(pos.z, -maxZ)) {
        return vec3(0, 0, 1);
    } else if (eq(pos.x, maxX)) {
        return vec3(-1, 0, 0);
    } else if (eq(pos.z, maxZ)) {
        return vec3(0, 0, -1);
    } else if (eq(pos.y, -maxY)) {
        return vec3(0, 1, 0);
    } else {
        return vec3(0, 0, 0);
    }
}

vec3 sphereNormal(vec3 pos)
{
    return normalize(pos - sphereCenter);
}

vec3 objKd(vec3 pos, float pickingID)
{
    if (isSpherePixel(pickingID)) {
        return 0.6 * sphereKd;
    }
    if (isPoolPixel(pickingID)) {
        return 0.6 * poolKd(pos);
    }
    if (isWaterSurfacePixel(pickingID)) {
        return waterKd;
    }
    return vec3(0.0);
}

bool inRect(vec3 pos, vec3 maxPos, int axis)
{
    bool result = true;
    for (int i = 0; i < 3; ++i) {
        if (i != axis) {
            result = (pos[i] <= maxPos[i] && pos[i] >= -maxPos[i]);
            if (!result) break;
        }
    }
    return result;
}

bool intersectRect(in Ray ray,vec3 maxPos, in int axis,
                   out float rayLen, out vec3 intersection)
{
    intersection = vec3(FLOAT_MAX);
    rayLen = (maxPos[axis] - (ray.o)[axis])/(ray.dir)[axis];
    if (rayLen < 0.0 || eq(rayLen, 0.0)) return false;
    intersection = ray.o + rayLen * ray.dir;
    return inRect(intersection, maxPos, axis);
}

bool intersectPool(in Ray ray, out float rayLen,
                   out vec3 normal, out vec3 kd,
                   out vec3 intersection)
{
    intersection = vec3(FLOAT_MAX);
    rayLen = FLOAT_MAX;
    normal = vec3(0.0, -1.0, 0.0);
    kd = vec3(0.0);

    float maxX = poolEdgeLen / 2.0;
    float maxY = poolHeight / 2.0;
    float maxZ =  poolEdgeLen / 2.0;
    vec2 texCoord = vec2(0.0, 0.0);

    if (intersectRect(ray, vec3(-maxX, maxY, maxZ), 0, rayLen, intersection)) { // left
        normal = vec3(1, 0, 0);
        texCoord = (intersection.yz + vec2(maxY, maxZ)) / tileTexWorldLen;
    } else if (intersectRect(ray, vec3(maxX, maxY, -maxZ), 2, rayLen, intersection)) { // back
        normal = vec3(0, 0, 1);
        texCoord = (intersection.xy + vec2(maxX, maxY)) / tileTexWorldLen;
    } else if (intersectRect(ray, vec3(maxX, maxY, maxZ), 0, rayLen, intersection)) { // right
        normal = vec3(-1, 0, 0);
        texCoord = (intersection.yz + vec2(maxY, maxZ)) / tileTexWorldLen;
    } else if (intersectRect(ray, vec3(maxX, maxY, maxZ), 2, rayLen, intersection)) { // front
        normal = vec3(0, 0, -1);
        texCoord = (intersection.xy + vec2(maxX, maxY)) / tileTexWorldLen;
    } else if (intersectRect(ray, vec3(maxX, -maxY, maxZ), 1, rayLen, intersection)) { // bottom
        normal = vec3(0, 1, 0);
        texCoord = (intersection.zx + vec2(maxZ, maxX)) / tileTexWorldLen;
    } else {
        return false;
    }
    kd = 0.6 * texture(tileTex, texCoord).rgb;
    return true;
}

bool intersectSphere(in Ray ray, out float rayLen,
                     out vec3 intersection)
{
    intersection = vec3(FLOAT_MAX);
    rayLen = FLOAT_MAX;

    vec3 D = normalize(ray.dir);
    vec3 E = ray.o;
    vec3 C = sphereCenter;
    float r = sphereRadius;
    float a = dot(D, D);
    float b = 2 * dot (D, E - C);
    float c = dot(E - C, E - C) - r * r;
    float disc = b * b - 4 * a * c; // discriminant
    if (disc < 0) return false;
    else if (disc == 0) {
        rayLen = -b/(2 * a);
    } else {
        rayLen = (-b - sqrt(disc)) / (2.0 * a);
    }
    if (rayLen < 0 || eq(rayLen, 0.0)) return false;

    intersection = E + rayLen * D;
    return true;
}

// incident ray: point to water surface point pos
// normal: should be in the same medium with incident ray
bool refractRay(in Ray iR, in vec3 pos, in vec3 normal, out Ray tR)
{
    tR = Ray(vec3(FLOAT_MAX), vec3(0.0, -1.0, 0.0));
    vec3 N = normalize(normal);
    vec3 I = normalize(iR.dir);
    float eta = 1.0 / n_water;
    if (I.y > 0) {
        eta = 1.0 / eta;
    }
    vec3 tDir = refract(I, N, eta);
    if (eq(length(tDir),0.0)) return false; // total internal reflection
    tR = Ray(pos, normalize(tDir));
    return true;
}

// incident ray: point to intersection "pos"
// normal: should be in the same medium with incident ray
bool reflectRay(in Ray iR, in vec3 pos, in vec3 normal, out Ray rR)
{
    rR = Ray(vec3(FLOAT_MAX), vec3(0.0, -1.0, 0.0));
    vec3 N = normalize(normal);
    vec3 I = normalize(iR.dir);
    vec3 rDir = normalize(reflect(I, N));
    rR = Ray(pos, rDir);
    return true;
}

// based on fresnel equation, https://en.wikipedia.org/wiki/Fresnel_equations
// normal is in the same medium with incidence
// i: incidence; t: refracted ray
// eta = n2 / n1
float calcReflectance (Ray i, Ray t,  vec3 normal)
{
    float cosT_i = dot(normalize(-i.dir.xyz), normalize(normal));
    float cosT_t = dot(normalize(t.dir.xyz), normalize(-normal));
    float n_1 = 1.0; // from air
    float n_2 = n_water; // to water
    if (i.dir.y > 0) {
        n_1 = n_water; // from water
        n_2 = 1.0; // to air
    }
    float n1CosTi = n_1 * cosT_i;
    float n2CosTt = n_2 * cosT_t;
    float n1CosTt = n_1 * cosT_t;
    float n2CosTi = n_2 * cosT_i;
    float R_s = pow(abs((n1CosTi - n2CosTt) / (n1CosTi + n2CosTt)), 2.0);
    float R_p = pow(abs((n1CosTt - n2CosTi) / (n1CosTt + n2CosTi)), 2.0);
    return 0.5 * (R_s + R_p);
}

vec2 waterSurfaceTexCoord(vec2 posOnPlane)
{
    float maxX = poolEdgeLen / 2.0;
    float maxZ =  poolEdgeLen / 2.0;
    vec2 texCoord = (posOnPlane + vec2(maxX, maxZ)) / poolEdgeLen;
    texCoord.y = 1 - texCoord.y;
    return texCoord;
}

float areaRatio(vec3 pos)
{
    vec3 dir2Light = normalize(-lightDir);
    vec2 posOnPlane = (pos + ((waterSurfaceH - pos.y)/dir2Light.y) * dir2Light).xz;
    vec2 texCoord = waterSurfaceTexCoord(posOnPlane);
    if (texCoord.x < 0.0 || texCoord.x > 1.0 || texCoord.y < 0.0 || texCoord.y > 1.0) return 0.0;
    return texture(causticsTex, texCoord).r;
}

vec3 waterPos(vec2 posOnPlane)
{
    float y = texture(waterPosTex, waterSurfaceTexCoord(posOnPlane)).r + waterSurfaceH;
    return vec3(posOnPlane.x, y, posOnPlane.y);
}

vec3 waterNormal(vec2 posOnPlane)
{
    return normalize(texture(waterNormalTex, waterSurfaceTexCoord(posOnPlane)).rgb);
}

// the intersection is on the plane y = waterSurfaceH;
bool intersectWaterSurface(in Ray ray, out float rayLen,
                           out vec3 intersection, out vec3 normal,
                           out vec3 kd, out Ray newRay)
{
    rayLen = FLOAT_MAX;
    intersection = vec3(FLOAT_MAX);
    normal = vec3(0.0, -1.0, 0.0);
    kd = vec3(0.0);
    newRay = Ray(intersection, normal);

    float maxX = poolEdgeLen / 2.0;
    float maxY = waterSurfaceH;
    float maxZ =  poolEdgeLen / 2.0;
    if (intersectRect(ray,vec3(maxX, maxY, maxZ), 1, rayLen, intersection)) {
        kd = waterKd;
        normal = waterNormal(intersection.xz);
        if (ray.dir.y > 0) {
            normal.y *= -1.0;
        }
        Ray tempR;
        if (!refractRay(ray, intersection, normal, tempR)) {
            reflectRay(ray, intersection, normal, tempR);
        }
        newRay = tempR;
        return true;
    }
    return false;
}

vec3 lightInDir(float cosTs)
{
    if (cosTs >= cosTp) return lightIMax;
    if (cosTs > cosTu) {
        float alpha = pow((cosTs - cosTu) / (cosTp - cosTu), lightExp);
        return alpha * lightIMax;
    }
    return vec3(0.0);
}

bool intersectLight(in Ray ray, out float rayLen,
                    out vec3 intersection, out vec3 emittance)
{
    rayLen = (lightPlaneH - ray.o.y) / ray.dir.y;
    intersection = vec3(FLOAT_MAX);
    emittance = vec3(0);
    if (rayLen < 0 || eq(rayLen, 0.0)) return false;

    vec3 p = ray.o.xyz + rayLen * ray.dir.xyz; // intersection
    vec3 L = normalize(p - lightPos);
    float cosTs = dot(L, normalize(lightDir));
    if (cosTs < cosTu) return false;

    vec3 lightI = lightInDir(cosTs);
    float alpha = clamp(dot(L, -ray.dir.xyz), 0.0, 1.0);
    emittance = alpha * lightI;
    return true;
}

// return true if intersection with obj is found;
// otherwise return false
bool castRay(in Ray ray, out float minRayLen,
             out vec3 nearestPos, out vec3 nearestNormal,
             out vec3 nearestKd)
{
    nearestPos = vec3(FLOAT_MAX);
    minRayLen = FLOAT_MAX;
    nearestNormal = vec3(0.0, -1.0, 0.0);
    nearestKd = vec3(0.0);

    vec3 intersection = nearestPos;
    float rayLen = minRayLen;
    vec3 normal = nearestNormal;
    vec3 kd = nearestKd;

    bool intersected = false;

    if (intersectPool(ray, rayLen, normal, kd, intersection)) {
        nearestPos = intersection;
        minRayLen = rayLen;
        nearestNormal = normal;
        nearestKd = kd;
        intersected = true;
    }
    if (intersectSphere(ray, rayLen, intersection)) {
        if (rayLen < minRayLen) {
            nearestPos = intersection;
            minRayLen = rayLen;
            nearestNormal = normal;
            nearestKd = 0.6 * sphereKd;
            intersected = true;
        }
    }
    return intersected;
}

vec3 ambient(vec3 pos, vec3 kd)
{
    float alphaAmbient = 2.0;
    float poolMaxY = poolHeight / 2.0;
    alphaAmbient -= clamp(1.0 - 2.0 * (pos.y + poolMaxY) / poolHeight, 0.0, 1.0);
    float dist2Sphere = length(sphereCenter - pos) + 0.05 * sphereRadius;
    alphaAmbient -= 1 / exp(dist2Sphere / sphereRadius - 1.0);
    return kd * alphaAmbient * AMBIENT;
}

float pcf(vec3 shadowCoords, int halfRegion, float bias)
{
    float intensity = 0.0;
    float dx = 1.0 / float(512);
    for (int x = - halfRegion; x <= halfRegion; ++x) {
        for (int y = - halfRegion; y <= halfRegion; ++y) {
            vec2 dxdy = vec2(x * dx, y * dx);
            float nearestDepth = texture(shadowMapTex, shadowCoords.xy + dxdy).z;
            float currentDepth = (shadowCoords.z + bias);
            intensity += step(currentDepth, nearestDepth);
        }
    }
    float sumMax = pow(float(2 * halfRegion + 1), 2.0);
    return intensity / sumMax;
}

float visibility(vec3 pos, float cosT)
{
    vec4 projLightSpace = shadowBiasVP * vec4(pos, 1.0);
    vec3 shadowCoords = projLightSpace.xyz / projLightSpace.w;
    shadowCoords = shadowCoords * 0.5 + vec3(0.5);
    return pcf(shadowCoords, 5, 0);

}

vec3 lightAt(vec3 pos, vec3 normal, vec3 kd, float causticsRatio)
{
    vec3 directLight = kd;

    if (pos.y < waterSurfaceH) directLight *= waterKd;
    vec3 N = normalize(normal);
    vec3 V = normalize(pos - camPos);
    vec3 R = normalize(reflect(V, N));
    vec3 L = normalize(lightPos - pos);
    float cosT = dot (N, L);
    Ray LR = Ray(pos, L);
    cosT = clamp(cosT, 0.0, 1.0);

    float rayLen;
    vec3 intersection;
    vec3 norm;
    vec3 iKd;
    vec3 emittance = vec3(0.0);

    intersectLight(LR, rayLen, intersection, emittance);
    directLight *= visibility(pos, dot (N, L))* emittance;

    return causticsRatio * directLight * cosT + ambient(pos, kd);
}

vec3 lighting(vec3 pos, vec3 normal, vec3 kd)
{
    vec3 newKd = kd;
    float h = waterPos(pos.xz).y;
    float causticsRatio = 1.0;
    if (pos.y < h) {
        causticsRatio = areaRatio(pos) + 1.0;
        newKd *= waterKd;
    }
    return lightAt(pos, normal, newKd, causticsRatio);
}

vec3 waterColor(vec3 pos, vec3 normal)
{
    Ray tR, rR;
    vec3 N = normalize(normal);
    vec3 V = normalize(pos - camPos);
    vec3 R = normalize(reflect(V, N));
    vec3 L = normalize(lightPos - pos);
    float cosT = clamp(dot (N, L), 0, 1);

    Ray viewRay = Ray(camPos, V);
    vec3 light = vec3(0.0);
    float reflectance = 1.0;
    float minRayLen, nearestAlpha;
    vec3 nearestPos, nearestNormal, nearestKd;

    if (refractRay(viewRay, pos, normal, tR)) {
        reflectance = calcReflectance(viewRay, tR, normal);
        if (castRay(tR, minRayLen, nearestPos, nearestNormal, nearestKd)) {
            light += (1.0 - reflectance) * lighting(nearestPos, nearestNormal, nearestKd);
        } else {
            vec3 emittance = vec3(0.0);
            if (intersectLight(tR, minRayLen, nearestPos, emittance)) {
                light += (1.0 - reflectance) * emittance;
            }
        }

    }
    reflectRay(viewRay, pos, normal, rR);
    if (castRay(rR, minRayLen, nearestPos, nearestNormal, nearestKd)) {
        light += reflectance * lighting(nearestPos, nearestNormal, nearestKd);
    } else {
        vec3 emittance = vec3(0.0);
        if (intersectLight(rR, minRayLen, nearestPos, emittance)) {
            light += reflectance * emittance;
        }
    }
    return light;
}

void main(void)
{
    vec3 pos = texture(worldPosTex, vsTexCoord).rgb;
    float pickingID = texture(pickIDTex, vsTexCoord).r;
    Ray viewRay = Ray(camPos, normalize(pos - camPos));
    if  (isWaterSurfacePixel(pickingID)) {
        vec3 normal = waterNormal(pos.xz);
        if (viewRay.dir.y > 0) {
            normal.y *= -1.0;
        }
        color = vec4(waterColor(pos, normal), 1.0);
    } else if (isPoolPixel(pickingID)) {
        vec3 kd = poolKd(pos);
        vec3 normal = poolNormal(pos);
        color = vec4(lighting(pos, normal, kd), 1.0);
    } else if (isSpherePixel(pickingID)) {
        vec3 kd = 0.6 * sphereKd;
        vec3 normal = sphereNormal(pos);
        color = vec4(lighting(pos, normal, kd), 1.0);
    } else {
        color = vec4(texture(sceneryTex, vsTexCoord).rgb, 1.0);
    }
}
