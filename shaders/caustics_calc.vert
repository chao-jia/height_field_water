#version 430 core
uniform int hmapSize;

layout (binding = 0) uniform sampler2D oldPosTex; // old position texture
layout (binding = 1) uniform sampler2D newPosTex; // new position texture

layout (location = 0) in ivec2 iPos;

out float oldPos;
out float newPos;

float normalizeCoord(int x)
{
    return (-1.0 + 2.0 * float(x) / float(hmapSize - 1));
}

float posOld(ivec2 texelCoord)
{
    ivec2 coord = ivec2(clamp(texelCoord.x, 0, hmapSize-1), clamp(texelCoord.y, 0, hmapSize-1));
    return texelFetch(oldPosTex, coord, 0).r;
}

float posNew(ivec2 texelCoord)
{
    ivec2 coord = ivec2(clamp(texelCoord.x, 0, hmapSize-1), clamp(texelCoord.y, 0, hmapSize-1));
    return texelFetch(newPosTex, coord, 0).r;
}

void main()
{
    gl_Position = vec4(normalizeCoord(iPos.x), normalizeCoord(iPos.y), -1.0, 1.0);
    oldPos = posOld(iPos);
    newPos = posNew(iPos);

}


