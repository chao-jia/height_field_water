#version 430 core
layout (binding = 0) uniform sampler2D posTex; // position texture
layout (binding = 1) uniform sampler2D velTex; // velocity texture
layout (location = 0) in ivec2 iPos;

uniform float deltaT;
uniform float cOVERh_squared;
uniform int hmapSize;
uniform float fMax;
uniform float velocityDampFactor;
uniform ivec2 rippleILoc;

uniform float worldEdgeLen;
uniform vec3 spherePos;
uniform vec3 lastSpherePos;
uniform float spehreRadius;
uniform float waterSurfaceH;

uniform float coSphereWave; // 0.12
uniform float coRipple; // 0.85

out float posNew;
out float velNew;

float normalizeCoord(int x)
{
    return (-1.0 + 2.0 * float(x) / float(hmapSize - 1));
}

float pos(ivec2 texelCoord)
{
    ivec2 coord = ivec2(clamp(texelCoord.x, 0, hmapSize-1), clamp(texelCoord.y, 0, hmapSize-1));
    return texelFetch(posTex, coord, 0).r;
}

vec3 worldPos(ivec2 pI)
{
    vec3 p;
    p.x = (float(pI.x) / float(hmapSize) - 0.5) * worldEdgeLen;
    p.z = (1 - float(pI.y) / float(hmapSize) - 0.5) * worldEdgeLen;
    p.y = pos(pI) + waterSurfaceH;
    return p;
}

bool sphereOnSurface(vec3 spherePos)
{
    return abs(spherePos.y - waterSurfaceH) < spehreRadius;
}

float sphereColLen(vec3 spherePos, ivec2 pI)
{
    vec3 p = worldPos(pI);
    vec3 delta = spherePos - p;
    if (length(delta.xz) >= spehreRadius) return 0.0;
    if (delta.y >= spehreRadius) return 0.0;
    float dx = p.x - spherePos.x;
    float dy = p.y - spherePos.y;
    float dz = p.z - spherePos.z;
    float disc = spehreRadius * spehreRadius - dx * dx - dz * dz;
    float t1= -dy + sqrt(disc);
    float t2 = -dy - sqrt(disc);
    if (t1 > 0.0) t1 = 0.0;
    return (t1 - t2);
}

float deltaColLen(ivec2 pI)
{
    if (!sphereOnSurface(spherePos) && !sphereOnSurface(lastSpherePos)) return 0.0;
    float currColLen = sphereColLen(spherePos, pI);
    float lastColLen = sphereColLen(lastSpherePos, pI);
    // return 0.12 * (currColLen - lastColLen);
    return coSphereWave * (currColLen - lastColLen);
}

float deltaRipple()
{
    float distToRipple = length(rippleILoc - iPos);
    float sizeOfRipple = 8.0;
    if ( distToRipple < sizeOfRipple) {
        float scale = pow((sizeOfRipple - distToRipple) / sizeOfRipple, 3);
        // return -co * 0.85;
        return - coRipple * 4.0 * scale;
    }
    return 0.0;
}

void main()
{
    gl_Position = vec4(normalizeCoord(iPos.x), normalizeCoord(iPos.y), -1.0, 1.0);
    float center = pos(iPos);
    float left = pos(iPos + ivec2(-1, 0));
    float right = pos(iPos + ivec2(1, 0));
    float up = pos(iPos + ivec2(0, -1));
    float down = pos(iPos + ivec2(0, 1));
    float f = cOVERh_squared * (left + right + up + down + deltaColLen(iPos) - 4.0 * center);
    f = clamp(f, -fMax, fMax);

    float vel = texelFetch(velTex, iPos, 0).r;
    velNew = (vel + f * deltaT);
    posNew = center + velNew * deltaT;

    velNew += deltaRipple();

    // damp
    float maxPos = 2;
    posNew = clamp(posNew, -maxPos, maxPos);
    float epsilon = 0.00001;
    if (abs(velNew) < epsilon &&  abs(posNew) < 10 * epsilon) {
        velNew = 0.0;
        posNew = 0.0;
    }
    velNew = velocityDampFactor * velNew;
    // posNew = velocityDampFactor * posNew;
}
