#version 430 core

layout (binding = 0) uniform sampler2D shadowMapTex;

in vec2 vsTexCoord;

layout (location = 0) out vec4 color;
layout (location = 1) out vec4 pickColor;
layout (location = 2) out vec3 worldPos;

vec3 colorFromDepth()
{
    float depth = texture(shadowMapTex, vsTexCoord).z;
    return vec3(1000.0 * (depth - 0.99785));
}

void main(void)
{
    color = vec4(colorFromDepth(), 1.0);
    pickColor = vec4(0, 0, 0, 1);
    worldPos = vec3(texture(shadowMapTex, vsTexCoord).z, 0, 0);
}
