#version 430 core
uniform float worldEdgeLen;
uniform int hmapSize;

layout (binding = 0) uniform sampler2D posTex; // position texture
layout (location = 0) in ivec2 iPos;
out vec3 normal;

float normalizeCoord(int x)
{
    return (-1.0 + 2.0 * float(x) / float(hmapSize - 1));
}

float height(ivec2 texelCoord)
{
    ivec2 coord = ivec2(clamp(texelCoord.x, 0, hmapSize-1), clamp(texelCoord.y, 0, hmapSize-1));
    return texelFetch(posTex, coord, 0).r;
}

void main(void)
{
    gl_Position = vec4(normalizeCoord(iPos.x), normalizeCoord(iPos.y), 0.0, 1.0);
    // normal = vec3(0.0, 1.0, 0.0);
    float texelSize = worldEdgeLen / float(hmapSize);
    vec3 currPos = vec3(iPos.x * texelSize, height(iPos + ivec2(0, 0)), iPos.y * texelSize);
    vec3 rightPos = vec3((iPos.x + 1) * texelSize, height(iPos + ivec2(1, 0)), iPos.y * texelSize);
    vec3 topPos = vec3(iPos.x * texelSize, height(iPos + ivec2(0, -1)), (iPos.y - 1) * texelSize);
    normal = normalize(cross(rightPos - currPos, topPos - currPos));
}
