#version 430 core
uniform float waterSurfaceH;

layout(location = 0) in vec2 position;
layout(location = 1) in vec2 texCoord;

out vec2 vsTexCoord;

void main(void)
{
    gl_Position = vec4(position.x, waterSurfaceH, position.y, 1.0);
    vsTexCoord = texCoord;
}
