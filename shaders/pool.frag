#version 430 core
uniform uint pickingID;

layout (binding = 0) uniform sampler2D tileTex;

in vec3 vWorldPos;
in vec3 vWorldNormal;
in vec2 vTexCoord;

layout (location = 0) out vec4 color;
layout (location = 1) out vec4 pickColor;
layout (location = 2) out vec3 worldPos;
layout (location = 3) out vec3 normal;

void main(void)
{
    color = vec4(texture(tileTex, vTexCoord).rgb, 1.0);
    pickColor = vec4(float(pickingID) / 255.0, 0.0, 0.0, 1.0);
    worldPos = vWorldPos;
    normal = vWorldNormal;
}
